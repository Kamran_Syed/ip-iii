<?php
/*
Plugin Name: Tracing Facility.
Plugin URI: www.agilesolutionspk.com
Description: 
Author: Agile solutions
Version: 1.0
Author URI: http://www.agilesolutionspk.com
*/

if ( !class_exists( 'Tracing_Facility' )){
	class Tracing_Facility{
		function __construct() {
			register_activation_hook( __FILE__, array($this, 'install') );
			add_action( 'admin_menu', array(&$this, 'admin_menu') );
		}
		
		function admin_menu(){
			add_menu_page('Find Trace', 'Find Trace', 'manage_options', 'agile_find_tracing_facility', array(&$this, 'find_tracing_facility') );
		}
		
		function find_tracing_facility(){
			if(isset($_POST['search_submit'])){
				$search_email = $_POST['search_email'];
				$search_order = $_POST['search_order'];
				if(!empty($search_email) && empty($search_order)){
					$result = $this->get_result_from_searching_email($search_email);
				}elseif(empty($search_email) && !empty($search_order)){
					$result = $this->get_result_from_searching_orderno($search_order);
				}elseif(! empty($search_email) && !empty($search_order)){
					$result = $this->get_result_from_searching($search_order, $search_email);
				}
			}
			?>
			<h1>Find Trace</h1>
			<div style = "float:left; clear:left;" >
				<form method = "post" action = "" >
					<div style = "float:left; clear:left;" >
						<div style = "float:left; width: 15em; margin-top:1em;" ><input style="width: 12em;" type = "text" name = "search_order" value = "" placeholder = "Search For OrderNo "  /> </div> 
						<div style = "float:left; width: 25em; margin-top:1em;" ><input style="width: 20em;" type = "text" name = "search_email" value = "" placeholder = "Search For Email "  /> </div> 
						<div style = "float:left; width: 10em; margin-top:1em;" ><input class = "button-primary" type = "submit" name = "search_submit" value = "Find Trace"  /> </div> 
					</div>
				</form>
			</div>
			<?php if(count($result) == 0 && isset($_POST['search_submit'])){
				?><div style = "float:left; clear:left;">Record Not Found</div><?php
				return ;
			}   ?>
			<?php if(!empty($search_order)) {?>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:15em;"><h3>Order No</h3></div>
				<div style = "float:left; "><h3><?php echo $search_order;?></h3></div>
			</div>
			<?php } ?>
			<?php if(!empty($search_email)) {?>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:15em;"><h3>Email</h3></div>
				<div style = "float:left; "><h3><?php echo $search_email;?></h3></div>
			</div>
			<?php } 
			if(!empty($result)){
				?>
				<div style = "float:left; clear:left;">
					<div style = "float:left; width:15em;">Date</div>
					<div style = "float:left;">Detail</div>
				</div>
				<?php
				foreach($result as $rs){
				$detail = unserialize($rs->detail);
				?>
				<div style = "float:left; clear:left;">
					<div style = "float:left; width:15em;"><?php echo $rs->dt; ?></div>
					<div style = "float:left;"><pre><?php print_r($detail); ?></pre></div>
				</div>
				<?php
				}
			}
		}
		
		function get_result_from_searching_email($search){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE email = '{$search}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_result_from_searching($search_order, $search_email){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE email = '{$search_email}' AND orderid  = '{$search_order}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_result_from_searching_orderno($search){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE orderid = {$search}";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function log_trace($email, $orderid, $detail){
			if(!is_email( $email )){
				return;
			}elseif(!is_array($detail)){
				return;
			}else{
				$result = $this->insert_log_trace_db($email, $orderid, $detail);
			}
			
		}
		

		function insert_log_trace_db($email, $orderid=0, $detail){
			global $wpdb;
			
			$date = new datetime();
			$dt = $date->format('Y-m-d H:i:s');
			$old = serialize ($detail);
			$sql = "INSERT INTO {$wpdb->prefix}_aspk_trace_log(dt,email,orderid,detail) VALUES('{$dt}','{$email}','{$orderid}','{$old}')";
			$wpdb->query($sql);	
		}
		
		function install(){
			global $wpdb;
			
			$sql="
				CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."_aspk_trace_log` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `dt` date NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `orderid` int(11) NULL,
				  `detail` TEXT NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$wpdb->query($sql);
		}
	}
}
$aspk_tracing_ip = new Tracing_Facility();
