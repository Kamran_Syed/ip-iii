<?php
/*
Plugin Name: Installment Plan 
Plugin URI: 
Description: It is used to record and show installments.
Author: Agile Solutions pk
Version: 4.6
Author URI: http://agilesolutionspk.com
*/
//global $msg_cart;
//function alog($a,$b,$c,$d){}
include_once __DIR__ .'/class-wc-authorize-net-cim-api-two.php';
if ( !class_exists( 'Agile_Installment_Plan' )){
	class Agile_Installment_Plan{

		function __construct(){
			add_action( 'admin_init', array(&$this, 'admin_init') );
			add_action('init', array(&$this, 'update_cart_action'));
			add_action( 'admin_menu', array(&$this, 'addAdminPage') );
			add_action('admin_enqueue_scripts', array(&$this, 'scripts_method') );
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action('admin_footer', array(&$this, 'admin_footer'));
			add_shortcode( 'agile_front_show', array(&$this, 'front_show_installment') );
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_filter('woocommerce_get_cart_item_from_session',  array(&$this,'cart_item_from_session'), 99, 3);
			add_filter( 'get_post_metadata', array(&$this, 'agile_post_meta_by_key'), 999, 4 );
			add_filter( 'manage_edit-shop_order_columns', array(&$this, 'my_columns_function'),99 );
			add_action( 'manage_shop_order_posts_custom_column', array(&$this, 'my_columns_values_function'),2 );
			add_filter( 'woocommerce_get_item_data', array(&$this,'item_data'), 10, 2 );
			add_action ('woocommerce_add_order_item_meta', array(&$this, 'add_item_meta'), 10, 2 );
			add_action ('woocommerce_order_status_processing', array(&$this, 'status_processing'), 10 );
			add_action( 'my_task_hook', array(&$this, 'prefix_do_this_daily') );
			add_action( 'prefix_hourly_event_hook', array(&$this, 'generate_csv_file_uploaded') );
			add_action( 'wp_ajax_nopriv_aspk_generate_csv', array(&$this,'generate_csv_file_uploaded' ));
			add_filter('cron_schedules', array(&$this, 'add_scheduled_interval') );
		}
		function add_scheduled_interval(){
			$schedules['minutes_5'] = array('interval'=>300, 'display'=>'Once 5 minutes');
			return $schedules;
		}
		function generate_csv_file_uploaded(){
			$process_total = get_option('_aspk_ip_fullcsv_total_process');
			$process_dt = get_option('_aspk_ip_fullcsv_dt_process');
			echo "<br>Total=".$process_total." Date=".$process_dt;
			$newfile = false;
			if($process_dt != date('Y-m-d')){
				$process_dt = date('Y-m-d');
				$process_total = 0;
				$newfile = true;
			}
			
			$totcust = $this->agile_show_all_installment();
			$remcust = $totcust - $process_total;
			echo " Total Cust=".$totcust." Remaining=".$remcust;
			If($remcust < 1 && $newfile){
				$upload_dir = wp_upload_dir();
				$upload_path = $upload_dir['basedir'];
				$csvtmpfile = $upload_path . "/dumyfile.csv";
				$csvlive	= $upload_path . "/studentsfull.csv";
				//$create_csv_size = filesize($csvtmpfile);
				//$download_csv_size = filesize($csvlive);
				//if($create_csv_size > $download_csv_size){
					if(file_exists($csvtmpfile)){
						rename($csvtmpfile,$csvlive);
						update_option ('_aspk_ip_fullcsv_total_process' , 0);
					}
				// }
				
				return;
			}
			$return_numbr_process = $this->process_csv_limited($process_total,$newfile);
			$process_total = $process_total + $return_numbr_process;
			echo " Processed=".$return_numbr_process." Total Processed=".$process_total."<br/>";
			update_option ('_aspk_ip_fullcsv_total_process' , $process_total);
			update_option ('_aspk_ip_fullcsv_dt_process' , $process_dt);
			?>
			<script>
				setTimeout(function(){
				   window.location.reload(1);
				}, 20000);
			</script>
			<?php
			
		}
		
		function process_csv_limited($limt_strt , $newfile){
			$n = 0;
			$all_data = $this->get_all_data_create_csv($limt_strt);
			$upload_dir = wp_upload_dir();
			$upload_path = $upload_dir['basedir'];
				$csvtmpfile = $upload_path . "/dumyfile.csv";
				$csvlive	= $upload_path . "/studentsfull.csv";
			if($newfile){
				$hdr = "WooCommerce ID, User ID, Student Name, Product, As, Amount, Status, Due Date, Paid Date, Auto, SKU \r\n";
				file_put_contents($upload_path . "/dumyfile.csv",$hdr);
			}
			foreach($all_data as $r){
				$orderid=$r->order_id;
				$user_id = $r->user_id;
					$email=get_post_meta($orderid);
					$mail=$email['_billing_email']['0'];
					$data = get_user_by('email',$mail );
					$user_name=$data->user_nicename;
					$user_fname = $data->user_firstname;
					$user_lname = $data->user_lastname;
					$user_name_display = $user_fname.' '.$user_lname;
					$prodid=$r->prod_id;
						$pord_get = get_product($prodid);
						if(!empty($pord_get)){
							$title = $pord_get->get_title();
						}
					if(!empty($pord_get)){
						$sku_pro = $pord_get->get_sku();
					}
					if($orderid)
					$y = $this->get_inst_count($orderid, $prodid);
					if($r->auto==''){
						$r->auto='No';
					}
					if($r->paydt==''){
						$r->paydt='0000-00-00';
					}
				 $content =  $orderid.','.$user_id.','.$user_name.','.$user_name_display.','.$title.',payment'.$r->installment.'of'.$y.','.$r->amount.','.$r->st.','.$r->duedt.','.$r->paydt.','.$r->auto.','.$sku_pro."\r\n";
				 file_put_contents($upload_path . "/dumyfile.csv",$content,FILE_APPEND);
				 $n++;
			}
			return $n;
		}
		function agile_post_meta_by_key($pre_val, $object_id, $meta_key, $single){
			if ( $meta_key != '_order_total' ) return $pre_val; /* Ignore everything else */
				$order = new WC_Order( $object_id );
				$total_coupon_amount = 0;
				$get_item = $order->get_items(); //get items
				foreach($get_item as $item){
					$pid = $item['product_id'];
					$pay = $item['pay'];
					if(empty($pay)) return $pre_val;
					$ptotal_pay=$pay+$ptotal_pay;
				}
				return 	$ptotal_pay;	
		}
		
		function aspk_logs($params, $setting){ // $params should be array
			$params ['Installment settings']= $setting;
			ob_start();
			$params = array_reverse($params);
			print_r($params);
			$get_content = ob_get_contents();
			file_put_contents(ABSPATH. "wp-content/plugins/Installments-plan/aspk-instllment.log",date('Y-m-d H:i:s').'   '.$get_content.PHP_EOL,FILE_APPEND);
		}
		
		function process_installment($oid){
			global $wpdb;
			
			$current_date = date('Y-m-d');
			$sql = "insert into {$wpdb->prefix}installment_process (dt,orderid) values('{$current_date}','{$oid}')";
			$wpdb->query($sql);
		}
		
		function delete_process_installment(){
			global $wpdb;
			
			$current_date = date('Y-m-d');
			$sql = "delete from {$wpdb->prefix}installment_process where dt < '{$current_date}'";
			$wpdb->query($sql);	
		}
		
		function chk_process_order($oid){
			global $wpdb;
			
			$sql = "select id from {$wpdb->prefix}installment_process where orderid = '{$oid}'";
			return $wpdb->get_var($sql);	
		}
		
		function prefix_do_this_daily(){
			$aspk_old = array();
			$detail = array();
			$spak = array();
			$spak1 = array();
			$this->delete_process_installment();
			$max_get_value = get_option('_agile_max_failed_attempt_trans', 0);
			$data = $this->agile_pay_cron_installment_gateway();
			$this->aspk_logs($spak['aspk_all_data'] = $data, 'all process order');
			$admin_email = get_settings('admin_email'); 
			foreach($data as $d){
				$chk_id_order = $this->chk_process_order($d->order_id);
				if(!empty($chk_id_order)) continue;
				$this->process_installment($d->order_id);
				$row_id = $d->Id;
				$user_id = $d->user_id;
				$meta_count_attmp = get_user_meta($user_id , 'agile_count_attmp_failed_transc');
				$spak1['single_order'] = $d;
				$spak1['agile_count_attmp_failed_transc'] = $meta_count_attmp;
				if($max_get_value <=  $meta_count_attmp){
					$order = $this->agile_pay_now_installment($row_id);
					$profile = get_user_meta( $user_id, '_wc_authorize_net_cim_profile_id');
					$spak1['profile_data'] = $profile;
					if(!empty($profile)){
						$customer_profile_id = $profile['0'];
						$profile_payment = get_user_meta( $user_id,'_wc_authorize_net_cim_payment_profiles');
						$spak1['profile_payment_data'] = $profile_payment;
						if(!empty($profile_payment)){
							$profile_id = $profile_payment['0'];
							foreach($profile_id as $key=>$id){
								$payment_profile_id=$key;
							}
							$get_txid = get_option( 'woocommerce_authorize_net_cim_settings' );
							$mode = $get_txid['test_mode'];
							if($mode == 'yes'){
								$environment = 'test';
								$api_transaction_key = $get_txid['test_api_transaction_key'];
								$api_user_id = $get_txid['test_api_login_id'];
							}else{
								$environment = 'production';
								$api_transaction_key = $get_txid['api_transaction_key']; 
								$api_user_id = $get_txid['api_login_id'];
							}
							$aspk_old['api_user_id'] = $api_user_id;
							$aspk_old['api_transaction_key'] = $api_transaction_key;
							$aspk_old['environment_mode'] = $environment;
							$aspk_old['order_id'] = $d->order_id;
							$aspk_user = get_user_by( 'id', $user_id );
							$mail = $aspk_user->user_email;
							$spak1['request_data_gate_way'] = $aspk_old;
							$detail_old['gateway_payment_deduct_request_ready'] = $aspk_old;
							//wp_mail( 'zeeshan.aspk@gmail.com', 'Check Mail', $d->order_id.'message send to request ready' );
							$this->log_trace($mail, $d->order_id, $detail_old);
							$agile_class_gateway = new WC_Authorize_Net_CIM_API_TO($api_user_id,$api_transaction_key,$environment);
							$resp = $agile_class_gateway->create_new_transaction($order,$payment_profile_id,$customer_profile_id);
							$code = $resp->messages;
							$code1 = $code->resultCode;
							$text = $code->message->text;
							$new_detail = array();
							$new_detail['messages'] = $code;
							$new_detail['code'] = $code1;
							$new_detail['message_text'] = $text;
							$spak1['gate_way_response'] = $aspk_response;
							$aspk_response  = print_r($resp , true);
							//$zee_maile = array('zeeshan.aspk@gmail.com' , 'studentaccounts@escoffieronline.com');
							$detail['gateway_payment_deduct_request_response_daily'] = serialize ( $aspk_response );
							//wp_mail( 'zeeshan.aspk@gmail.com', 'Check Gateway Response', $aspk_response );
							$this->log_trace($mail, $d->order_id, $detail);
							if($code == 'ok' || $text == 'Successful.'){
								$this->update_total_installment_pay_now_cron($row_id);
								$mail_array = array($admin_email,$mail);
								$amount = $d->amount;
								wp_mail( $mail_array, 'Amount Deduction', 'The Amount has been deducted from your Credit Card for this'."\n".'Installment No:'.$d->installment.''."\n".'Invoice Number:'.$d->order_id.''."\n".'Customer ID: '.$d->user_id.''."\n".'Amount:'.$amount);
								update_user_meta( $user_id, 'agile_count_attmp_failed_transc', 0 );
							}else{
								if(empty($meta_count_attmp)){
									 $meta_count_attmp =  1;
								}else{
									$meta_count_attmp = $meta_count_attmp++;
								}
								wp_mail('studentaccounts@escoffieronline.com', 'Credit Card Credential Failed', 'Deducted payment has been failed from your Credit Card for this'."\n".'Installment No:'.$d->installment.''."\n".'Order Number:'.$d->order_id.''."\n".'Email:'.$mail."\n".'Amount:'.$amount."\n".'Payment Gateway Message:'.$text );	
								update_user_meta( $user_id, 'agile_count_attmp_failed_transc', $meta_count_attmp );
							}
						}else{
							$detail_old1['profile_payment_empty_no_send_request_to_gateway'] = $profile_payment;
							$aspk_user = get_user_by( 'id', $user_id );
							$mail = $aspk_user->user_email;
							$this->log_trace($mail, $d->order_id, $detail_old1);
							//wp_mail( $mail_array, ' Payment Profile', $payment_profile_id);
						}
					}else{
							$detail_old2['profile_empty_no_send_request_to_gateway'] = $profile;
							$aspk_user = get_user_by( 'id', $user_id );
							$mail = $aspk_user->user_email;
							$this->log_trace($mail, $d->order_id, $detail_old2);
						//wp_mail( $mail_array, 'Customer Profile', 'Customer Profile Not Found.'."\n".'Invoice Number:'.$d->order_id);
					}
				}else{
					$detail_old3['decline_exceed_no_send_request_to_gateway'] = $meta_count_attmp;
					$aspk_user = get_user_by( 'id', $user_id );
					$mail = $aspk_user->user_email;
					$this->log_trace($mail, $d->order_id, $detail_old3);
					wp_mail( $admin_email, 'Decline Exceed', 'Customer Profile Not Found/Invalid Credit Card.'."\n".'Invoice Number:'.$d->order_id);
				} // handle chk max attmp failed transaction
				$this->aspk_logs($spak1, 'Request or Response');
			}	
		}
			function show_credit_card_reports(){
				$dt1 = new DateTime();
				$today_plus_3_mnth = $dt1->add(new DateInterval('P3M'));
				?>
				<div id = "cc_expire">
				 	<div style= "float: left; clear: left;">
						<h3>Report show Less Than 3(month) of Credit Card Expires.</h3>
					</div>
					<div class="agile-content"><!-- start table div -->
						<div class="agile-header">
							<div class="agile-as-cred">User Name</div>
							<div class="agile-from">Card No</div>
							<div class="agile-due">Expire Date</div>
							<div class="agile-status">Profile Id</div>
							<div class="agile-status">Status</div>
						</div>
						<?php
						 $args = array(
							'blog_id'      => $GLOBALS['blog_id']
						 ); 
						$users = get_users($args);
						foreach($users as $user){
							$user_id = intval($user->ID);
							if($user_id == 0) continue;
							$profile = get_user_meta( $user_id, '_wc_authorize_net_cim_profile_id' , true);
							$profile_payment = get_user_meta( $user_id,'_wc_authorize_net_cim_payment_profiles', true);
							if(!empty($profile_payment)){
								foreach($profile_payment as $key=>$pf){
								$dtexp = DateTime::createFromFormat('m/y', $pf['exp_date'] );
									if($dtexp > $today_plus_3_mnth){
										$color = "red";
										$status = "Expired";
									}else{
										$color = "green";
										$status = "Not Expired";
									}
									if($dtexp < $today_plus_3_mnth){
										?>
										<div class="agile-table">
											<div class="agile-as-cred">&nbsp;<?php echo $user->display_name; ?> </div>
											<div class="agile-from">&nbsp;<?php  if(!empty($pf['last_four'])){echo 'XXX'.$pf['last_four'];}elseif(!empty($pf['card_number'])){
												$cn = substr($pf['card_number'],  -4);
												echo 'XXX'.$cn;
											}?></div>
											<div class="agile-due">&nbsp;<?php echo $pf['exp_date'];?></div>
											<div class="agile-status" >&nbsp;<?php echo $profile;?></div>
											<div class="agile-status" style = "color: <?php echo $color;?>;"><?php echo $status;?></div>
										</div>
										<?php
									}
								}
							}
						} ?>
					</div><!-- end table div -->	
				</div> <!--ccexpire-->
	<?php 	}
		function show_missing_credit_card_info(){
			?>
			<div id = "cc_expire">
				<div style= "float: left; clear: left;">
					<h3>Report show Less Than 3(month) of Credit Card Expires.</h3>
				</div>
				<div class="agile-content"><!-- start table div -->
					<div class="agile-header">
						<div class="agile-as-cred">User Name</div>
						<div class="agile-from">Email</div>
						<div class="agile-status-report">Payment Profile</div>
					</div>
					<?php
					 $args = array(
						'blog_id'      => $GLOBALS['blog_id']
					 ); 
					$users = get_users ( $args );
					
					foreach ( $users as $user ) {
						$user_id = intval ( $user->ID );
						$profile_payment = get_user_meta( $user_id,'_wc_authorize_net_cim_payment_profiles', true);
						if(empty($profile_payment)){ ?>
						<div class="agile-content">
							<div class="agile-as-cred"><?php echo $user->display_name.'</br>';?></div>
							<div class="agile-from"><?php echo $user->user_email;?></div>
							<div class="agile-status-report">Not Found</div>
						</div><?php
						}
					} ?>
				</div><!-- end table div -->	
			</div> 
			<?php
		}
		//END FUNCTION CSV FIELDS VALIDATION.
		//SUB MENU FUNCTION MISSING CREDIT CARD
		function show_missing_profile(){
			?>
			<div id = "cc_expire">
				<div style= "float: left; clear: left;">
					<h3>Report show Less Than 3(month) of Credit Card Expires.</h3>
				</div>
				<div class="agile-content"><!-- start table div -->
					<div class="agile-header">
						<div class="agile-as-cred">User Name</div>
						<div class="agile-from">Email</div>
						<div class="agile-status-report">Profile</div>
					</div>
					<?php
					 $args = array(
						'blog_id'      => $GLOBALS['blog_id']
					 ); 
					$users = get_users ( $args );
					
					foreach ( $users as $user ) {
						$user_id = intval ( $user->ID );
						$profile = get_user_meta( $user_id, '_wc_authorize_net_cim_profile_id' , true);
						if(empty($profile)){ ?>
						<div class="agile-content">
							<div class="agile-as-cred"><?php echo $user->display_name.'</br>';?></div>
							<div class="agile-from"><?php echo $user->user_email;?></div>
							<div class="agile-status-report">Not Found</div>
						</div><?php
						}
					} ?>
				</div><!-- end table div -->	
			</div> 
			<?php
		}
		//END FUNCTION.
			function agile_csv_field_validate($line){
				$arr = explode(",",$line);
				$arr_count = count($arr);
					if($arr_count != 7){
						throw new Exception('Invalid number of fields');
					}
					if($arr[0] != 'userid'){
						if(preg_match('/\s/',$arr[0])){
							throw new Exception('User Id Have Space(s) or User Id did not match');
						}
						if(!is_numeric($arr[2]) && $arr[2] > 0){
							throw new Exception('Invalid Amount Due');
						}
						if(!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$arr[1])){
							throw new Exception('Invalid Email');
						}
						if (!is_int($arr[3]) && $arr[3] <= 0){
							throw new Exception('Invalid No of Installments');
						}
						$d = DateTime::createFromFormat("m/d/Y",$arr[4]);
						if(!$d){
							throw new Exception('Invalid Date Format');
						}
						if($arr[5] == 'Yes' || $arr[5] == 'No'){
							$int = intval($arr[6]);	
							$order =$this->chk_order_id_already_exist($int);
							if($order) throw new Exception('Order Id is already exists');
						if(is_int($int)){
							
						}else{
							throw new Exception('Invalid Order Id');
						}	
						}else{
							throw new Exception('Not Auto');
						}
					}
				return true;
			}
			function chk_order_id_already_exist($order_id){
				global $wpdb;
				
				$sql = "SELECT order_id FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id={$order_id} ";
				$rs = $wpdb->get_row($sql); 
				return $rs;
			}

		//zeeshan
		function status_processing($order_id,$x='',$y=''){
			$this->process_order($order_id);
		}
		function process_order($order_id){
			$order = new WC_Order( $order_id );
			$order_id=$order->id;		
			foreach( $order->get_items() as $item ) {
				$prod_id = $item['product_id'];                    //product id
				$pay = $item['pay'];  									 //paid
				if(!$pay) return;
				$line_total = $item['line_total'];  						  //total amount
				$coupon_amount = $item['total_coupon_amount'];
				$line_total = $line_total - $coupon_amount;
				$remi=$item['remi'];								  //remaining Installment
				$auto='Yes';
				$remi1=$remi+1;
				$remi2=$remi1-$remi;
				$total_reminder=$line_total-$pay;                   //remaining amount
				$per_installment=$total_reminder/$remi;           //per installment
				$meta_values = get_post_meta( $order_id );
				$get_txid=get_option( 'woocommerce_authorize_net_cim_settings' );
				$mode=$get_txid['test_mode'];
				if($mode=='yes'){
					$api_transaction_key=$get_txid['test_api_transaction_key'];
				}else{
					$api_transaction_key=$get_txid['api_transaction_key'];
				}
				$user_id=$meta_values['_customer_user']['0'];						//user id
				$pay_mthd=$meta_values['_payment_method_title']['0']; 				//user id
				$dt= new datetime(); 											 //today date
				$dt=$dt->format('Y-m-d');
				$this->agile_insert_netpayment($remi2,$order_id,$pay,$api_transaction_key,$prod_id,$dt,$user_id,$pay_mthd,$auto);
				$this->agile_create_installment($remi1,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$auto,$pay);
			}
		}	
		function agile_create_installment($remi1,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$auto,$pay){
			$detail = array();
			$detail['total_installment_due'] = $remi1;
			$detail['per_installment_payment'] = $per_installment;
			$detail['transaction_key'] = $api_transaction_key;
			$detail['auto'] = $auto;
			$detail['status'] = 'due';
			if(!empty($pay)){
				if(!empty($remi1)){
					for ($x=2; $x<=$remi1; $x++){
						$date = new DateTime($dt);
						$y = $x -1;
						$date->add(new DateInterval('P'.$y .'M'));
						$install_dt=$date->format('Y-m-d');
						$this->agile_insert_installment($install_dt,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$x,$auto);
					}	
				}
			}else{
				for ($x=1; $x<=$remi1; $x++){
					$date = new DateTime($dt);
					$y = $x -1;
					$date->add(new DateInterval('P'.$y .'M'));
					$install_dt=$date->format('Y-m-d');
					$this->agile_insert_installment($install_dt,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$x,$auto);
				}	
			}
		}	
		function agile_insert_netpayment($remi2,$order_id,$pay,$api_transaction_key,$prod_id,$dt,$user_id,$pay_mthd,$auto){
			global $wpdb;
			
			$sql = "insert into {$wpdb->prefix}agile_ip_installment (user_id,amount,txid,order_id,prod_id,installment,auto,pay_mthd,dt,duedt,paydt,st) values('{$user_id}','{$pay}','{$api_transaction_key}','{$order_id}','{$prod_id}','{$remi2}','{$auto}','{$pay_mthd}','{$dt}','{$dt}','{$dt}','paid')";
			$wpdb->query($sql);
			$detail = array();
			$detail['sql'] = $sql;
			$user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_insert_installment($install_dt,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$x,$auto){
			global $wpdb;
			
			$dt= new datetime(); 
			$dt=$dt->format('Y-m-d');
			$sql = "insert into {$wpdb->prefix}agile_ip_installment (user_id,amount,txid,order_id,prod_id,installment,auto,dt,duedt,st) values('{$user_id}','{$per_installment}','{$api_transaction_key}','{$order_id}','{$prod_id}','{$x}','{$auto}','{$dt}','{$install_dt}','due')";
			$wpdb->query($sql);
			$detail = array();
			$detail['sql'] = $sql;
			$user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}	
		function agile_schedule_installment($o_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id={$o_id} order by Id asc";
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_schedule_installment_history($o_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id={$o_id} AND st = 'paid'";
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_schedule_installment_due_all($o_id){
			global $wpdb;
			
			$sql = "SELECT Id FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id={$o_id} AND st = 'due'";
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_schedule_installment_process($o_id,$prod_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id='{$o_id}' AND prod_id = '{$prod_id}' order by duedt desc";
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_show_debug_installment(){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_log_installment_get_cron`";
			$rs = $wpdb->get_results($sql); 
			return $rs;
		}
		function agile_schedule_installment_amount($id){
			global $wpdb;
			
			$sql = "SELECT amount FROM `{$wpdb->prefix}agile_ip_installment` WHERE Id='{$id}'";
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_pay_now_installment($row_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment` WHERE Id={$row_id}" ;
			$rs=$wpdb->get_results($sql); 
			return $rs;
		}
		function agile_pay_cron_installment_gateway(){
			global $wpdb;

			$sql = "SELECT *
					FROM `{$wpdb->prefix}agile_ip_installment`
					WHERE `duedt` > now() - INTERVAL 15 DAY 
					AND duedt <= now()
					AND auto = 'Yes'
					AND st = 'due' ";
			$rs=$wpdb->get_results($sql);
			return $rs;
		}
		function agile_match_rowid_userid($row_id,$user_id){
			global $wpdb;
			
			$sql = "SELECT count( * )
					FROM {$wpdb->prefix}agile_ip_installment
					WHERE Id ={$row_id} && user_id ={$user_id}" ;
			$rs=$wpdb->get_var($sql); 
			return $rs;
		}
		function agile_show_due_installment($order_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where order_id=$order_id and st='due' order by duedt asc";
			$rs= $wpdb->get_results($sql);
			return $rs;
		}
		function agile_show_all_installment(){
			global $wpdb;
			
			$sql = "SELECT count(*) FROM {$wpdb->prefix}agile_ip_installment";
			$rs= $wpdb->get_var($sql);
			return $rs;
		}
		function get_all_data_create_csv($st){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment limit {$st},1000";
			$rs= $wpdb->get_results($sql);
			return $rs;
		}
		function agile_show_front_installment($user_id){
			global $wpdb;
			
			//$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where user_id=$user_id and st='due' order by duedt and amount asc";
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where user_id=$user_id and st='due' order by order_id desc, installment asc";
			$rs= $wpdb->get_results($sql);
			return $rs;
		}	
		function agile_show_outstandin_installment($o_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where order_id=$o_id and st='paid' order by paydt desc";
			$rs= $wpdb->get_results($sql);
			return $rs;
		}
		function agile_change_outstandin_installment($o_id,$chng_prod_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where order_id= {$o_id} and prod_id = {$chng_prod_id} and st='due' ";
			$rs= $wpdb->get_results($sql);
			return $rs;
		}
		function agile_get_first_due_installment($o_id, $prod_id){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment where order_id=$o_id and prod_id = $prod_id and st='due' order by duedt asc";
			$rs= $wpdb->get_row($sql);
			return $rs;
		}	
		function get_inst_count($orderid, $prodid){
			 global $wpdb;
		
			$sql = "SELECT count(`order_id`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$orderid} and prod_id={$prodid}";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}
		function get_inst_count_due($orderid,$prod_id){
			 global $wpdb;
		
			$sql = "SELECT count(`order_id`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$orderid} and prod_id = {$prod_id} and st = 'due'";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}	
		function get_total_installment($o_id){
			global $wpdb;
		
			$sql = "SELECT sum(`amount`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$o_id}";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}
		function get_total_installment_remainig($o_id,$product_id){
			global $wpdb;
		
			$sql = "SELECT sum(`amount`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$o_id} and prod_id = {$product_id} and st = 'due'";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}
		function get_total_due($order_id){
			global $wpdb;
		
			$sql = "SELECT sum(`amount`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$order_id} AND st = 'due'";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}
		function update_total_installment($amnt,$b){
			global $wpdb;
			
			$dt= new datetime(); 											   //today date
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set amount = {$amnt} where `Id`={$b}";
			$wpdb->query($sql);
			$detail = array();
			$detail['update_total_installment_amount'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function update_total_installment_pay_now($row_id){
			global $wpdb;
			
			$dt= new datetime();
			$dt=$dt->format('Y-m-d');			//today date
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set  paydt = '{$dt}',st = 'paid' where `Id`={$row_id}";
			$wpdb->query($sql);
			$detail = array();
			$detail['update_total_installment_amount_fornt_end_user'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function update_total_installment_pay_now_cron($row_id){
			global $wpdb;
			
			$dt= new datetime();
			$dt=$dt->format('Y-m-d');			//today date
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set  paydt = '{$dt}',st = 'paid' where `Id`={$row_id}";
			$wpdb->query($sql);
			$detail = array();
			$detail['daily_cron'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_update_st_auto($rid,$ap){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set auto ='{$ap}' where `Id`={$rid}";
			$wpdb->query($sql);
			$detail = array();
			$detail['admin_update_auto_status'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_update_st($rid){
			global $wpdb;
			
			$dt= new datetime(); 											   //today date
			$dt=$dt->format('Y-m-d');
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set  paydt = '{$dt}' , st = 'paid'   where `Id`={$rid}";
			$wpdb->query($sql);
			$detail = array();
			$detail['admin_update_paid_date'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_update_due_dt($dt,$rid){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set  `duedt` = '{$dt}' where `Id`='{$rid}'";
			$wpdb->query($sql);
			$detail = array();
			$detail['admin_update_due_date'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_update_st_due($rid){
			global $wpdb;
			
			//$dt= new datetime(); 											   //today date
			//$dt=$dt->format('Y-m-d');
			$dt="";
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set auto ='Yes' , paydt = '{$dt}' , st = 'due'   where `Id`={$rid}";
			$wpdb->query($sql);
			$detail = array();
			$detail['admin_update_due_status'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_update_auto_pay_due($a_id,$auto_val){
			global $wpdb;
			
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment set auto = '{$auto_val}' where `Id`={$a_id}";
			$wpdb->query($sql);
			$detail = array();
			$detail['admin_update_due_status'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
		}
		function agile_sum_paid_amount(){
			global $wpdb;
			
			$sql = "SELECT order_id,prod_id,st, sum(amount) as amt FROM {$wpdb->prefix}agile_ip_installment  where st='paid' group by order_id, prod_id";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		function agile_count_paid_month($oid , $pid){
			global $wpdb;
			
			$sql = "SELECT COUNT(*) FROM {$wpdb->prefix}agile_ip_installment  where order_id = {$oid} and prod_id = {$pid} and st = 'paid'";
			$rs = $wpdb->get_var($sql);
			return $rs;
		}
		
		function cart_item_from_session( $data, $values, $key ) {
			$data['pay'] = isset( $values['pay'] ) ? $values['pay'] : '';
			$data['remi'] = isset( $values['remi'] ) ? $values['remi'] : '';
			$data['Id'] = isset( $values['Id'] ) ? $values['Id'] : '';
			return $data;
		}
		function update_cart_action() {
			global $woocommerce;
			
			if(! session_id()){
				session_start();
			}
			if ( ( ! empty( $_POST['update_cart'] ) || ! empty( $_POST['proceed'] ) ) && wp_verify_nonce($_POST['agile_nonce'],'agile_cart')) {
				$cart_totals = isset( $_POST['cart'] ) ? $_POST['cart'] : '';
				if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
					foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
						if ( isset( $cart_totals[ $cart_item_key ]['pay'] ) ) {
							$woocommerce->cart->cart_contents[ $cart_item_key ]['pay'] = $cart_totals[ $cart_item_key ]['pay'];
							$woocommerce->cart->cart_contents[ $cart_item_key ]['remi'] = $cart_totals[ $cart_item_key ]['remi'];
							$woocommerce->cart->cart_contents[ $cart_item_key ]['Id'] = $cart_totals[ $cart_item_key ]['Id'];
						}
					}
				}
			}
		}
		
		function add_caps(){
			$role = get_role( 'administrator' );
			$role->add_cap( 'aspk' ); 
		}
		function admin_init(){	
			$this->add_caps();
			
		}
		
		function find_tracing_facility(){
			if(isset($_POST['search_submit'])){
				$search_email = $_POST['search_email'];
				$search_order = $_POST['search_order'];
				if(!empty($search_email) && empty($search_order)){
					$result = $this->get_result_from_searching_email($search_email);
				}elseif(empty($search_email) && !empty($search_order)){
					$result = $this->get_result_from_searching_orderno($search_order);
				}elseif(! empty($search_email) && !empty($search_order)){
					$result = $this->get_result_from_searching($search_order, $search_email);
				}
			}
			?>
			<h1>Find Trace</h1>
			<div style = "float:left; clear:left;" >
				<form method = "post" action = "" >
					<div style = "float:left; clear:left;" >
						<div style = "float:left; width: 15em; margin-top:1em;" ><input style="width: 12em;" type = "text" name = "search_order" value = "" placeholder = "Search For OrderNo "  /> </div> 
						<div style = "float:left; width: 25em; margin-top:1em;" ><input style="width: 20em;" type = "text" name = "search_email" value = "" placeholder = "Search For Email "  /> </div> 
						<div style = "float:left; width: 10em; margin-top:1em;" ><input class = "button-primary" type = "submit" name = "search_submit" value = "Find Trace"  /> </div> 
					</div>
				</form>
			</div>
			<?php if(count($result) == 0 && isset($_POST['search_submit'])){
				?><div style = "float:left; clear:left;">Record Not Found</div><?php
				return ;
			}   ?>
			<?php if(!empty($search_order)) {?>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:15em;"><h3>Order No</h3></div>
				<div style = "float:left; "><h3><?php echo $search_order;?></h3></div>
			</div>
			<?php } ?>
			<?php if(!empty($search_email)) {?>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:15em;"><h3>Email</h3></div>
				<div style = "float:left; "><h3><?php echo $search_email;?></h3></div>
			</div>
			<?php } 
			if(!empty($result)){
				?>
				<div style = "float:left; clear:left;">
					<div style = "float:left; width:15em;">Date</div>
					<div style = "float:left;">Detail</div>
				</div>
				<?php
				foreach($result as $rs){
				$detail = unserialize($rs->detail);
				?>
				<div style = "float:left; clear:left;">
					<div style = "float:left; width:15em;"><?php echo $rs->dt; ?></div>
					<div style = "float:left;"><pre><?php print_r($detail); ?></pre></div>
				</div>
				<?php
				}
			}
		}
		
		function addAdminPage() {
			add_menu_page('Installment', 'Installment', 'manage_options', 'agile_installment_plan', array(&$this, 'agile_admin_show_installment') );
			add_submenu_page('', 'Payment', 'Payment', 'create_users', 'agile_show_users_payment', array(&$this, 'agile_admin_show_payment'));
			add_submenu_page('', 'Payment Schedule', 'Payment Schedule', 'create_users', 'agile_schedule_payment', array(&$this, 'agile_payment_schedule'));
			add_submenu_page('', 'paynow', 'paynow', 'manage_options', 'agile_ip_paynow', array(&$this, 'paynow'));
			add_submenu_page('agile_installment_plan', 'Installment Settings', 'Installment Settings', 'create_users', 'agile_settings_installment', array(&$this, 'settings_Installments'));
			add_submenu_page('', 'Outstanding Payment', 'Outstanding Payment', 'create_users', 'agile_show_users_outstanding', array(&$this, 'admin_show_installment'));
			add_submenu_page('agile_installment_plan', 'Installment Full CSV', 'Installment Full CSV', 'create_users', 'agile_show_get_profile', array(&$this, 'get_customer_profile'));
			add_submenu_page('agile_installment_plan', 'Installment Summary', 'Installment Summary', 'create_users', 'agile_show_get_report', array(&$this, 'show_installment_report'));
			add_submenu_page('agile_installment_plan', 'Installment Activity', 'Installment Activity', 'create_users', 'agile_show_get_history', array(&$this, 'show_history'));
			add_submenu_page('agile_installment_plan', 'Import students', 'Import students', 'create_users', 'agile_upload_student_history', array(&$this, 'uplaod_csv_student'));
			add_submenu_page('', 'Enter Installments', 'Enter Installments', 'create_users', 'agile_show_enter_amount', array(&$this, 'admin_self_create_installment'));
			//add_submenu_page('agile_installment_plan', 'Test', 'Test', 'create_users', 'agile_settings_installment_test', array(&$this, 'test_function'));
			add_submenu_page('agile_installment_plan', 'Credit Card Reports', 'Credit Card Reports', 'create_users', 'agile_settings_installment_credit_reports', array(&$this, 'show_credit_card_reports'));
			//add_submenu_page('', 'Test1', 'Test1', 'create_users', 'agile_settings_installment_test_log', array(&$this, 'agile_test_log'));
			add_submenu_page('', 'Reset Meta User Value', 'Reset Meta User Value', 'create_users', 'agile_settings_reset_user_failed_trans', array(&$this, 'agile_reset_user_count_failed_transaction'));
			add_submenu_page('agile_installment_plan', 'Show Missing Profile', 'Show Missing Profile', 'create_users', 'agile_show_missing_credit_card_info', array(&$this, 'show_missing_profile'));
			add_submenu_page('agile_installment_plan', 'Show Missing Payment Profile', 'Show Missing Payment Profile', 'create_users', 'agile_show_missing_payment_profile', array(&$this, 'show_missing_credit_card_info'));
			add_submenu_page('agile_installment_plan', 'For Developer Only', 'For Developer Only', 'create_users', 'agile_for_developer_only', array(&$this, 'generate_csv_file_uploaded'));
			add_menu_page('Find Trace', 'Find Trace', 'manage_options', 'agile_find_tracing_facility', array(&$this, 'find_tracing_facility') );
		}
		
		function test_function(){
			
			/* $array = array(1,2,3,4,5,6,7,8,9);
			foreach($array as $ar){
				$this->process_installment($ar);
			} */
			$chk_id_order = $this->chk_process_order(11);
			if(!empty($chk_id_order)) continue;
			$this->delete_process_installment();
		}
		
		function agile_reset_user_count_failed_transaction(){
			update_user_meta( $_GET['user_id'], 'agile_count_attmp_failed_transc', 0 );
			?>
				<div style = "float:left; clear:left; color:green; background-color:white;">User Attempt Value Updated Successfully</div>
			<?php
		}
		function upload_file_handle(){
			if ( ! function_exists( 'wp_handle_upload' ) ) require_once( ABSPATH . 'wp-admin/includes/file.php' );
			$uploadedfile = $_FILES['banerfile'];
			
			$upload_overrides = array( 'test_form' => false );
			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			
			if ( $movefile ) {
				$path = $movefile['file'];
				$file_name = basename($path); 
				$x = array();
				$handle = fopen($path, "r");
				$n=0;
				if ($handle) {
					while (($line = fgets($handle)) !== false) {
						$n++;
						try{
							$this->agile_csv_field_validate($line);
							$this->admin_create_installment_exist_old_user($line);
						}catch(Exception $e){
							echo 'Error on Line '.$n.' '.$line.'   '.$e->getMessage().'<br/>';
						}
					}
				} else {
					echo "File Not Found";
				} 
				fclose($handle);
			}
		}
		function uplaod_csv_student(){
			if(isset($_FILES['banerfile'])){
					?>
						<h1>Processing CSV File</h1><br/>
						<h4>Errors will be shown I as go</h4>
					<?php
					$this->upload_file_handle();
					return;
			}
			?>
				<div class = "tw-bs container">
					<form action="" method="post" enctype="multipart/form-data">
						<div class = "row">
							<div class = "col-md-12">
								<div class="form-group">
								<h4>Upload CSV FILE:</h4>
									<input type="file" name="banerfile" id="banner_file"/>
								</div>
							</div>
						</div>
						<div class = "row">
							<div class = "col-md-2">
								<input class="btn btn-primary" type = "submit" name = "update_setting" id = "update_setting" value = "Upload"/>
							</div>
						</div>
					</form>
				</div>
			<?php
		}
		function admin_create_installment_exist_old_user($line){
			$line_info = explode(",",$line);
			 $args = array(
				'blog_id'      => $GLOBALS['blog_id']
			 ); 
			$users = get_users($args);
			foreach($users as $user){
				if( $user->display_name == $line_info[0]){
					$email = $user->user_email;
					$user_id = $user->ID;
					$order = new WC_Order( $line_info[6] );
					foreach( $order->get_items() as $item ) {
						$remi =	$line_info[3];				  //remaining Installment
						$auto = $line_info[5];
						$total_reminder = $line_info[2];			//remaining amount
						$per_installment = $total_reminder/$remi;  //per installment
						$prod_id = $item['product_id'];           //product id
						$pay = '';
						$meta_values = get_post_meta( $line_info[5] );
						$get_txid=get_option( 'woocommerce_authorize_net_cim_settings' );
						$mode=$get_txid['test_mode'];
						if($mode=='yes'){
							$api_transaction_key=$get_txid['test_api_transaction_key'];
						}else{
							$api_transaction_key=$get_txid['api_transaction_key'];
						}
						$detail = array();
						$detail['total_installment_paid'] = $remi;
						$detail['payment'] = $per_installment;
						$detail['transaction_key'] = $api_transaction_key;
						$detail['auto'] = $auto;
						$detail['status'] = 'paid';
						$detail_old['admin_create_installment_exist_user'] = $detail;
						$this->log_trace($email, $line_info[6], $detail_old);
						$dt= new datetime($line_info[4]); 
						$dt=$dt->format('Y-m-d');
						$this->agile_create_installment($remi,$per_installment,$api_transaction_key,$line_info[6],$prod_id,$dt,$user_id,$auto,$pay);
						break;
					}
				}
			}
		}
		function show_installment_report(){
			?>
			<h3>Reports Paid Installment</h3>
			<div class="agile-content">
				<div class="agile-header">
					<div class="Payment_agile-due">WC Id</div>
					<div class="payment_agile-auto">Student Name</div>
					<div class="payment_agile-as">Product</div>
					<div class="payment_agile-as">Sale Amount</div>
					<div class="payment_agile-auto">Total Paid to Date</div>
					<div class="payment_agile-auto">Month Date to Paid</div>
				</div>
				<div class="agile-table">
				<?php
				$add=0;
				$row = $this->agile_sum_paid_amount();
				foreach($row as $r){
					$y=$r->amount;
					$add=$y+$add;
					$x = get_product($r->prod_id);
					if(!empty($x)){
						$title=$x->get_title();
						$price_prod = $x->get_price();
					}
					$orderid=$r->order_id;
					$count_paid_month = $this->agile_count_paid_month($orderid,$r->prod_id);
					$email=get_post_meta($orderid);
					$mail=$email['_billing_email']['0'];
					$data = get_user_by('email',$mail );
					$user_name=$data->user_nicename;
					$user_fname = $data->user_firstname;
					$user_lname = $data->user_lastname;
					$user_name_display = $user_fname.' '.$user_lname;
						?>
					<div class="agile-table">
						<div class="Payment_agile-due">&nbsp;<?php echo $r->order_id;?></div>
						<div class="payment_agile-auto"> &nbsp;<?php  echo $user_name_display;?> </div>
						<div class="payment_agile-as">&nbsp;<?php  echo $title;?></div>
						<div class="payment_agile-as">&nbsp;<?php  echo $price_prod;?></div>
						<div class="payment_agile-auto">&nbsp;<?php  echo $r->amt;?></div>
						<div class="payment_agile-auto">&nbsp;<?php  echo $count_paid_month;?></div>
					</div>
		<?php	} 	?>
				</div>
			</div>
			<?php
		}
		function agile_test_log(){	
			?>
			<h3>Show Data</h3>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:12em;">Date</div>
				<div style = "float:left;">Data</div>
			</div>
			<?php
			$show_data = $this->agile_show_debug_installment();
			foreach($show_data as $show){
				 $show_d = unserialize($show->data);
				 echo "<pre>";
				?>
			<div style = "float:left; clear:left;">
				<div style = "float:left; width:12em;color:red;"><?php echo $show->date; ?></div>
				<div style = "float:left;"><?php print_r($show_d); ?></div>
			</div>
				<?php
			}
				 

		}
		function admin_self_create_installment(){
			$detail = array();
			$detail['paynow'] = $_POST;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
			if(isset($_POST['admin_entry'])){
				$order_id = $_POST['oid'];
				$user_id = $_POST['uid'];
				$prod_id = $_POST['pid'];
				$prod_total = $_POST['prod_preice'];
				$installments = $_POST['install_pay'];
				$installments_add = $installments + 1;
				$initial_payment = $_POST['init_pay'];
				$api_transaction_key = '';
				$remaing_payment = $prod_total- $initial_payment;
				$per_installment = $remaing_payment/$installments;
				$auto = "Yes";
				$dt= new datetime(); 				//today date
				$dt=$dt->format('Y-m-d');
				$nextday = date('Y-m-d', strtotime(' +1 day'));
				global $wpdb;
				
					$sql = "insert into {$wpdb->prefix}agile_ip_installment (user_id,amount,order_id,prod_id,installment,auto,dt,duedt,st) values('{$user_id}','{$initial_payment}','{$order_id}','{$prod_id}','1','{$auto}','{$dt}','{$nextday}','due')";
					$wpdb->query($sql);
					$detail = array();
					$detail['admin_self_create_installment'] = $sql;
					$user_id = get_current_user_id();
					$user_id = $user_info = get_userdata($user_id);
					$email = $user_info->user_email;
					$this->log_trace($email, $order_id, $detail);
				$this->agile_create_installment($installments_add,$per_installment,$api_transaction_key,$order_id,$prod_id,$dt,$user_id,$auto,$initial_payment);
			}
			$order = new WC_Order( $_GET['id']);
				$get_item   = $order->get_items(); //get items
				$email 	    = get_post_meta($_GET['id']);
				$mail 		= $email['_billing_email']['0'];
				$data   	= get_user_by('email',$mail );
				$user_name  = $data->user_nicename;		
				$user_id    = $data->ID;
				?>
					<h3>Enter Installments</h3>
					<?php 
						if(!empty($installments)){
							$pord_get = get_product($prod_id);
							$title_prod    = $pord_get->get_title();
							?>
							<div class = "admin_installement_report_show">You are added successfuly Initial payment =<?php echo $initial_payment; ?>, Installments =<?php echo $installments; ?>, Order No =<?php echo $order_id;?> And Product = <?php echo $title_prod;?></div>
							<?php
						}
					?>
					<div class ="agile-header">
					<div class = "admin_order">Order No#</div>
					<div class = "admin_order">Product Total</div>
					<div class = "admin_product">Product</div>
					<div class = "admin_product">Client</div>
					<div class = "admin_initial">Initial Payment</div>
					<div class = "admin_installement">Installments</div>
					<div class = "admin_buton_entry">&nbsp;</div>
					</div>
				<?php
				foreach($get_item as $item){
					$pid     = $item['product_id'];
					$p_total = $item['line_total'];
					$x       = get_product($pid);
					$title   = $x->get_title();
					?>
					<form method = "post" action = "">
						<div class ="agile-table-admin">
							<input type = "hidden" name = "oid" value = "<?php echo $_GET['id']; ?>" />
							<div class = "admin_order"><?php echo $_GET['id'];?></div>
							<input type = "hidden" name = "pid" value = "<?php echo $pid; ?>" />
							<div class = "admin_order"><?php echo $p_total;?></div>
							<input type = "hidden" name = "prod_preice" value = "<?php echo $p_total; ?>" />
							<div class = "admin_product"><?php echo $title ;?></div>
							<input type = "hidden" name = "uid" value = "<?php echo $user_id; ?>" />
							<div class = "admin_product"><?php echo $user_name; ?></div>
							<div class = "admin_initial">
								<input type = "text" required  size = "9" name = "init_pay" value = "" />
							</div>
							<div class = "admin_installement">
								<input type = "number" required name = "install_pay" value = "" />
							</div>
							<div class = "admin_buton_entry">
								<input type = "submit" name = "admin_entry" value = "Save" class = "button-primary"/>
							</div>
						</div>
					</form>
					<?php
				}
		}
		function arrange($id){   //for showing installment data
			global $wpdb;
			$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment WHERE Id ={$id} AND st = 'due'";
			$rs = $wpdb->get_row($sql);
			return $rs;
		}
		function paynow(){
			/* echo "Paynow works";*/
			//echo admin_url(); 
			
			global $wpdb;
			
			$id = $_GET['id'];
			
			if(empty($id)){
				echo "Error, Invalid Installment";
				return;
			}
			
			$sql = "UPDATE {$wpdb->prefix}agile_ip_installment  SET pay_mthd = 'admin', paydt = now(), st = 'paid' WHERE Id = {$id}";
			
			$x = $wpdb->query($sql);
			$detail = array();
			$detail['paynow'] = $sql;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
			
			if($x){
				$sql = "SELECT * FROM {$wpdb->prefix}agile_ip_installment WHERE Id = {$id}";
				$bilal = $wpdb->get_row($sql);
				$x = get_product($bilal->prod_id);
				$title = $x->get_title();
				$order_id = $bilal->order_id; 
				?>
				<h4>Installment Paid Successfully.</h4>
				<div style= "clear:left; float:left;"><?php echo 'Order id: '.$order_id;?></div>
				<div style= "clear:left; float:left;"><?php echo 'Installment No: '.$bilal->installment;?></div>
				<div style= "clear:left; float:left;"><?php echo 'Product Name: '.$title;?></div>
				<div style= "clear:left; margin-top:2em; float:left;">
				<?php $url = admin_url('admin.php?page=agile_show_users_payment', __FILE__); ?>
				
				<a href="<?php echo $url.'&&id='.$order_id;?>">
					<input type="button" size="8"  value="Show User Payment" class="button-primary" />
				</a>
				</div>
				<?php
			}else{
				?>
				<h4>An Error Occurred While Paying Installment.</h4>
				<?php
			}
			
		}
		function agile_admin_show_installment(){
		
		}
		function install_get_record($st,$lt){
			global $wpdb;
			
			$sql="Select * from {$wpdb->prefix}agile_ip_installment order by order_id desc limit {$st},{$lt} ";
			$results = $wpdb->get_results($sql);
			return $results;
		}
		function rows_count_installment_table(){
			global $wpdb;
			
			$sql="Select COUNT(*) from {$wpdb->prefix}agile_ip_installment";
			$rowscount = $wpdb->get_var($sql);
			return $rowscount;
		}
		function get_customer_profile(){
			global $post;
			
			//$show=$this->agile_show_all_installment();
			$lt = 100;
			$rowscount = $this->rows_count_installment_table();
			if( isset($_GET['st'] ) ){
				$st = $_GET['st'];
			}else{
				$st=0;
			}
			if($st > $lt){
				$pst = $st - $lt;
			}else{
				$pst = 0;
			}
			$show = $this->install_get_record($st,$lt);
			$permalink = get_permalink( $post->ID );
			$url = plugins_url('order_installment_csv.php?st='.$st, __FILE__);
			$csvurl = home_url().'/wp-content/uploads/studentsfull.csv';
			?>
				<div class="agile-heading"><h3> Outstanding Payment</h3></div>
				<div class="agile-csv-full"><a href="<?php  echo $csvurl; ?>"><input class="button-primary"type="button" value="Download Full CSV"/></a></div>
				<div class="agile-content">
					<div class="agile-header">
							<div class="agile-due">Due</div>
							<div class="agile-as">As</div>
							<div class="agile-auto">Auto</div>
							<div class="agile-from">From</div>
							<div class="agile-for">Product</div>
							<div class="agile-status">Status</div>
							<div class="agile-amount">Amount</div>
					</div>
						<?php
						foreach($show as $s){
							$email=get_post_meta($s->order_id);
							$mail=$email['_billing_email']['0'];
							$data = get_user_by('email',$mail );
							$user_name=$data->user_nicename;
							$x = get_product($s->prod_id);
							$x=$x->post;
							$title=$x->post_title;
							$orderid=$s->order_id;
							$prodid=$s->prod_id;
							$count= $this->get_inst_count($orderid, $prodid);
							?>
							<div class="agile-table">
								<div class="agile-due"><?php echo $s->duedt;?></div>
								<div class="agile-as">Payment&nbsp;&nbsp;<?php echo $s->installment;?> of <?php echo $count;?> </div>
								<div class="agile-auto">&nbsp;<?php  echo $s->auto;?></div>
								<div class="agile-from">&nbsp;<?php echo $user_name;?></div>
								<div class="agile-for">&nbsp;<?php echo $title;?></div>
								<div class="agile-status">&nbsp;<?php echo $s->st;?></div>
								<div class="agile-amount">&nbsp;<?php echo $s->amount;?></div>
							</div>
							<?php
						} 	?>
					<div class="main_pagination">
						<?php if(($st >= $lt)){ ?>	
								<div class="ul" style = "float:left;width:10em;"> <a href="<?php echo $permalink.'?page=agile_show_get_profile&st='.$pst?>"> Previous </a></div>
						<?php }
						if($rowscount > $st){
							$st = $st +$lt;
						?>
						<div class="ul"> <a href="<?php echo  $permalink.'?page=agile_show_get_profile&st='.$st;?>"> Next </a></div>
						<?php } ?>			
					</div>
				</div>	

			<?php
		}
		function get_pages_html($selected_page_id, $s){
            //echo $selected_page_id;
            $args = array('posts_per_page' => 1000, 'post_type'=> 'page', 'post_status'=> 'publish');
            $all_pages = get_posts( $args );
            ?>
            <select name="<?php echo $s;?>" id="<?php echo $s;?>" >
                <option value="all">---  select page ---</option>
                <?php    
                foreach($all_pages as $r){
                    $selected = "";
                    if(isset($selected_page_id)){
                        if($selected_page_id == $r->ID) $selected = " selected ";
                    }
                    ?>
                    <option value="<?php echo $r->ID;?>" <?php echo $selected;?>><?php echo $r->post_title;?> </option>
                    <?php
                }
                ?>
            </select>
            <?php
		}
		function remove_shortcode($id,$s){
            $p_base_page = get_post($id);
            $p_getpage = $p_base_page->post_content;
            $p_setpage = str_replace($s, "", $p_getpage);
            $p_base_page->post_content = $p_setpage;
            wp_update_post( $p_base_page );
        }
        function add_shortcode($id,$s){
            $base_page = get_post($id);
            $getpage = $base_page->post_content;
            $setpage = str_replace($s, "", $getpage);
            $setpage .= $s;
            $my_post = array('ID'=>$id,'post_content'=>$setpage);
            wp_update_post( $my_post );        
        }
		function settings_Installments(){
            $x = get_option('sfsdfsd',array());
            $x['thankyou'] = 19;
            $x['button'] = 13;
            $x['createpetition'] = 14;
            update_option('sfsdfsd',$x);
			$detail = array();
			$detail['post'] = $_POST;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
            if(isset($_POST)){
                if(isset($_POST['setting_page'])){
                    //delete short-code from previous page (button)
                    $previous_bpage = get_option('_agile_installment_billa', -1);
                    $this->remove_shortcode($previous_bpage,'[agile_front_show]');
                    //add short-code to new page (button)
                    $new_base_button = $_POST['base_button'];
                    $this->add_shortcode($new_base_button,'[agile_front_show]');
                    update_option( '_agile_installment_billa', $new_base_button );
                }
            } 
			if(isset($_POST['max_val_att'])){
				$max_attmpt = $_POST['max_attmpt'];
				 update_option( '_agile_max_failed_attempt_trans', $max_attmpt );
			}
			$max_get_value = get_option('_agile_max_failed_attempt_trans', 0);
            ?>
            <div class="pri_container">
                <h1>Settings</h1><hr/>
                <form action="" method="post">
                    <div class="sec_container">
                        <h3>Installments Base Page Setup</h3>
                        <div class="row">
                            <div class="col_right">                                
                                <?php
                                $selected_bpage = get_option('_agile_installment_billa', -1);
                                echo $this->get_pages_html($selected_bpage,'base_button');
                                ?>                                        
                            </div>
                        </div>
  
                    </div><br/><br/>
                    <hr/>
                    <input type="submit" name="setting_page" value="Save changes" class="button-primary">                
                </form>
            </div>
			 <div class="pri_container">
                <h2>Max Value Of Falied Transaction Attempt</h2><hr/>
                <form action="" method="post">
                    <div class="sec_container">
                        <h3>Set Value</h3>
                        <div class="row">
                            <div class="col_right">                                
                               <input type = "text" name = "max_attmpt" size = "4" value = "<?php echo $max_get_value; ?>" />                                     
                            </div>
                        </div>
  
                    </div><br/><br/>
                    <hr/>
                    <input type="submit" name="max_val_att" value="Save" class="button-primary">                
                </form>
            </div>
            <?php
			
        }
		function agile_payment_schedule(){
			global $wpdb;
			$o_id=$_GET['id'];
			$detail = array();
			$detail['change_installment_admin'] = $_POST;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
			//  post handle total amount or installment change
			if(isset($_POST['change_installment_admin'])){
				$chng_amnt = $_POST['change_total']; 
				$chng_install = $_POST['change_installment'];
				$chng_prod_id = $_POST['product_id_admin'];
				$per_installment = $chng_amnt/$chng_install ;
				$all_installment_due = $this->agile_change_outstandin_installment($o_id,$chng_prod_id);
				foreach($all_installment_due as $al_inst_due){
					$row_id_del_onebyone = $al_inst_due->Id;
					$sql="Delete from {$wpdb->prefix}agile_ip_installment where Id='{$row_id_del_onebyone}'";
					$wpdb->query($sql);
					$detail = array();
					$detail['delete_installment'] = $sql;
					$user_id = get_current_user_id();
					$user_id = $user_info = get_userdata($user_id);
					$email = $user_info->user_email;
					$this->log_trace($email, $order_id, $detail);
				}	
				$order_renew = new WC_Order( $o_id );
				foreach( $order_renew->get_items() as $item ) {
					$remi = $chng_install;								  //remaining Installment
					$auto='Yes';
					$pay=$item['pay'];
					if(!empty($pay)){
						$remi1=$remi+1;
					}else{
						$remi1=$remi;
					}
					$total_reminder = $chng_amnt;                   //remaining amount
					$per_installment = $total_reminder/$remi;           //per installment
					$prod_id = $chng_prod_id;                    //product id
					$meta_values = get_post_meta( $o_id );
					$get_txid=get_option( 'woocommerce_authorize_net_cim_settings' );
					$mode=$get_txid['test_mode'];
					if($mode=='yes'){
						$api_transaction_key=$get_txid['test_api_transaction_key'];
					}else{
						$api_transaction_key=$get_txid['api_transaction_key'];
					}
					$user_id=$meta_values['_customer_user']['0'];						 //user id
					$pay_mthd=$meta_values['_payment_method_title']['0']; 
					$dt= new datetime(); 											   //today date
					$dt=$dt->format('Y-m-d');
					$this->agile_create_installment($remi1,$per_installment,$api_transaction_key,$o_id,$prod_id,$dt,$user_id,$auto,$pay);
					break;
				}
				?><div style = "color:green;">Your Record Updated Successfully.</div><?php
			}
			//  post handle total amount or installment change
			$get=$this->get_total_installment($o_id);
			If(isset($_POST['submit'])){
				$res_process=$this->agile_schedule_installment($o_id);
				$install=$_POST['amount'];//post amount
				$count = 0;
				$id=$_POST['id'];//post ids
				$autopayment = $_POST['autopayment'];
				$duedt = $_POST['duedt'];
				$autopay = $_POST['autopay'];
				foreach($duedt as $key=>$dt){
					$index=$key;
					$rid=$id[$index];
					$this->agile_update_due_dt($dt,$rid);	
				}
				foreach($autopayment as $key=>$ap){
					if($ap=='No'){
						$index=$key;
						$rid=$id[$index];
						$this->agile_update_st_auto($rid,$ap);
					}
					if($ap=='Yes'){
						$index=$key;
						$rid=$id[$index];
						$this->agile_update_st_auto($rid,$ap);
					}
				}
				foreach($autopay as $key=>$a){
					if($a=='Yes'){
						$index=$key;
						$rid=$id[$index];
						$this->agile_update_st($rid);
					}
					if($a == 'No'){
						$index=$key;
						$rid=$id[$index];
						$this->agile_update_st_due($rid);
					}
				}
				$n=0;

				foreach($install as $in){
					$n=$in+$n;
				}
				$n	 = round($n, 2); 
				$n	 = floatval($n);
				$get = floatval($get);
				if($n == $get){ 
					$a=0;
					foreach($install as $amnt){
						$b=$id[$a];
						$a++;
						$this->update_total_installment($amnt,$b);
					}
					?><h4 style="color:green;">Installment Updated Successfully.</h4><?php
				}else{
					?><h4 style="color:red;">Total Amount Of Installments Differs</h4><?php
				}	 
			}
			if(isset($_POST['delete_install'])){
	
				$row_del = $_POST['delete_id'];
				$order_id_del = $_POST['delete_order_id'];
				$pord_id_del = $_POST['delete_prod_id'];
				$amount_add = $this->agile_get_first_due_installment($order_id_del,$pord_id_del);
				$amount = $amount_add->amount;
				$updt_id = $amount_add->Id;
				$amount_del = $_POST['delete_amount'];
				$change_amount = $amount + $amount_del;
				$this->update_total_installment($change_amount,$updt_id);
				$sql="Delete from {$wpdb->prefix}agile_ip_installment where Id='{$row_del}'";
				$wpdb->query($sql);
				$detail = array();
				$detail['delete_installment'] = $sql;
				$user_id = get_current_user_id();
				$user_id = $user_info = get_userdata($user_id);
				$email = $user_info->user_email;
				$this->log_trace($email, $order_id, $detail);
				?>
				<div style = "color:green;">Your Record Updated Successfully.</div>
				<?php
			}
			$all_id = $this->agile_schedule_installment_due_all($o_id);
			if(isset($_POST['all_auto_pay'])){
				$auto_val = $_POST['all_auto'];
				foreach($all_id as $ai){
					$a_id = $ai->Id;
					$this->agile_update_auto_pay_due($a_id,$auto_val);
				}
			}
			$results=$this->agile_schedule_installment($o_id);
			$url = plugins_url('order_installment_csv.php?', __FILE__);
			$url_new = admin_url('admin.php?page=agile_show_get_history&', __FILE__);
			$order_renew_add_field = new WC_Order( $o_id );
			?>
			<div>
			<div class="agile-heading"><h3>Payment Schedule</h3></div>
			<div class="agile-total">
				<div class="agile-pay_total">Total Amount</div>
				<div class="agile-amount_total" style = "color:blue;"><?php echo $get;?></div>
			</div>
			<?php foreach( $order_renew_add_field->get_items() as $item ) {
				$pro_id=$item['product_id'];
				$remaing_total_payment = $this->get_total_installment_remainig($o_id,$pro_id);
				$due_installment_count = $this->get_inst_count_due($o_id,$pro_id);
			?>
				<form method = "post" action = "">
					<div class="agile-total">
						<div class="agile-pay_total">Remaining Total Amount</div>
						<div class="agile-amount_total">
							<input type = "text" name = "change_total" size = "5" value = "<?php echo $remaing_total_payment;?>"/>
							<input type = "hidden" name = "product_id_admin"  value = "<?php echo $pro_id;?>"/>
						</div>
						<div class="agile-amount_total">
							<input type = "text" size = "5" name = "change_installment" value = "<?php echo $due_installment_count;?>"/>
						</div>
						<div class="agile-amount_total">
							<input type = "submit" name = "change_installment_admin" class = "button-primary" value = "save"/>
						</div>
					</div>
				</form>
			<?php } ?>
			<div class="agile-total">
				<form method = "post" action = "">
					<div class="agile-amount_total" style = "margin-top:1em; margin-bottom:1em;">
						<select name = "all_auto">
							<option>Select</option>
							<option value = "Yes">All Auto</option>
							<option value = "No">All None</option>
						</select>
					</div>
					<div class="agile-amount_total" style="margin-top:1em; margin-bottom:1em; margin-left:2em;" >
						<input  class="button-primary" type = "submit" name = "all_auto_pay" value = "AutoPay" />
					</div>
				</form>
			</div>
			<div class="agile-csv"><a href="<?php  echo $url.'id='.$o_id;?>"><input class="button-primary"type="button" value="Download CSV"/></a></div>
			</div>
			<div class="agile-content">
				<div class="agile-header">
					<div class="schedule-agile-due">Due Date</div>
					<div class="schedule_agile-auto">Auto Payments</div>
					<div class="schedule_agile-as">Payment Status</div>
					<div class="schedule-agile-due">Paid Date</div>
					<div class="schedule_agile-for">Description</div>
					<div class="schedule_agile-amount">Amount</div>
					<div class="schedule_agile-amount">&nbsp;</div>
				</div>
				<?php 
				$n=0;
				foreach($results as $r){
					$orderid=$r->order_id;
					$prodid=$r->prod_id;
					$cnt_inst= $this->get_inst_count($orderid, $prodid);
					if($r->st==paid){
						$x="readonly";
					}else{
						$x='';
					}
					//set autopay yes/no
					$apchecked='';
					if($autopay[$n] == 'No') $apchecked="checked";				
					?>
					<form method="post" action="">
						<div class="agile-header">
							<div class="schedule-agile-due">
								<input type="text" size="10"  name="duedt[]" <?php echo $x;?> value="<?php echo $r->duedt;?>" />
							</div>
							<div class="schedule_agile-auto"><?php// if($r->auto=='Yes'){ echo $r->auto;}else{ echo 'No';}?>
							<?php if($r->auto == Yes){?>
								<select name="autopayment[]">
										<option ><?php echo $r->auto;?></option>
										<option <?php echo $apc;?> value="No">No</option>
								</select>
							<?php }else{?>
								<select name="autopayment[]">
										<option>No</option>
										<option  value="Yes">Yes</option>
								</select>
							<?php } ?>
							</div>
							<div class="schedule_agile-as">
							<?php if($r->st == paid){?>
								<select name="autopay[]">
									<option <?php echo $apchecked;?>><?php echo $r->st;?></option>
									<option value="No">due</option>
								</select>
							<?php }else{?>
								<select name="autopay[]">
									<option <?php echo $apchecked;?>>due</option>
									<option value="Yes">paid</option>
								</select>
							<?php } ?>
								
							</div>
							<div class="schedule-agile-due">
								<input type="text" size="10" name="paydt" readonly <?php echo $x;?> value="<?php echo $r->paydt;?>" />
							</div>
							<div class="schedule_agile-for">
								<input type="text" size="20" readonly name="desc" <?php echo $x;?> value="Payment&nbsp;&nbsp; <?php echo $r->installment;?>&nbsp;of <?php echo $cnt_inst;?>" />
							</div>
							<div class="schedule_agile-amount">
								<input type="text" size="5"name="amount[]" <?php echo $x;?> value="<?php echo $r->amount;?>" />
								<input type="hidden" size="5"name="id[]" value="<?php echo $r->Id;?>" />
							</div>
							<div class="schedule_agile-amount">
								<input type="hidden" name="delete_id" value="<?php echo $r->Id;?>" />
								<input type="hidden" name="delete_order_id" value="<?php echo $orderid;?>" />
								<input type="hidden" name="delete_prod_id" value="<?php echo $prodid;?>" />
								<input type="hidden" name="delete_amount" value="<?php echo $r->amount;?>" />
								<input type="submit" class = "button-primary" name="delete_install"  value="Delete" />
							</div>
						</div>	
						<?php	
				}	
						?>
						<input type="hidden" name="total"  value="<?php echo $n;?>" />
						<div class="pay-button">
							<input type="submit" size="5" name="submit"  value="Update" class="button-primary" />
							<a href = "<?php  echo $url_new.'id='.$o_id;?>"><input type="button" size="5" name="activity"  value="Activity" class="button-primary" /></a>
						</div>
					</form>
			</div>
				<?php 
		}
		function show_history(){
			global $wpdb;
			
			$detail = array();
			$detail['refund_amount'] = $_POST;
			$user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
			$o_id = $_GET['id'];
			$results = $this->agile_schedule_installment_history($o_id);
			if(isset($_POST['refund_amount'])){
				$exist_id = $_POST['refund_id'];
				$exist_amount = $_POST['refund_amount_pay'];
				$exist_payment = $_POST['refund_payment'];
				$refund = $_POST['refund'];
				$dt= new datetime(); 							 //today date
				$dt=$dt->format('Y-m-d');
				$sql = "insert into {$wpdb->prefix}agile_refund_installment (row_id,dt,amount,installment,status) values('{$exist_id}','{$dt}','{$exist_amount}','{$exist_payment}','{$refund}')";
				$wpdb->query($sql);	
				$detail = array();
				$detail['delete_installment'] = $sql;
				$user_id = get_current_user_id();
				$user_id = $user_info = get_userdata($user_id);
				$email = $user_info->user_email;
				$this->log_trace($email, $order_id, $detail);
			}
			
		?>
			<h3>Activity</h3>
			<div class="agile-content"><!--content start -->
				<div class="agile-header">
					<div class="agile-due">Order No</div>
					<div class="agile-as"><?php echo $results[0]->order_id; ?></div>
					<div class="agile-auto"></div>
					<div class="agile-from"></div>
					<div class="agile-for"></div>
					<div class="agile-status"></div>
					<div class="agile-amount"></div>
					<div class="agile-amount">&nbsp;</div>
				</div>
				<div class="agile-header">
					<div class="agile-due">Create Date</div>
					<?php $date = $results[0]->dt;?>
					<?php $newdt =  explode(' ',$date);?>
					<?php $dt_n = $newdt[0];?>
					<div class="agile-as"><?php echo $dt_n; ?></div>
					<div class="agile-auto"></div>
					<div class="agile-from"></div>
					<div class="agile-for"></div>
					<div class="agile-status"></div>
					<div class="agile-amount"></div>
					<div class="agile-amount">&nbsp;</div>
				</div>
				<div class="agile-header">
					<div class="agile-due">paid Date</div>
					<div class="agile-as">Payment</div>
					<div class="agile-auto">&nbsp;</div>
					<div class="agile-from">From</div>
					<div class="agile-for">Product</div>
					<div class="agile-status">Status</div>
					<div class="agile-amount">Amount</div>
					<div class="agile-amount">&nbsp;</div>
				</div>
				<?php
				foreach($results as $s){
					$email=get_post_meta($s->order_id);
					$mail=$email['_billing_email']['0'];
					$data = get_user_by('email',$mail );
					$user_name=$data->user_nicename;
					$x = get_product($s->prod_id);
					$x=$x->post;
					$title=$x->post_title;
					$orderid=$s->order_id;
					$prodid=$s->prod_id;
					$count= $this->get_inst_count($orderid, $prodid);
					?>
					<div class="agile-table">
						<div class="agile-due"><?php echo $s->paydt;?></div>
						<div class="agile-as">Payment&nbsp;&nbsp;<?php echo $s->installment;?> / <?php echo $count;?> </div>
						<div class="agile-auto">&nbsp;<?php//  echo $s->auto;?></div>
						<div class="agile-from">&nbsp;<?php echo $user_name;?></div>
						<div class="agile-for">&nbsp;<?php echo $title;?></div>
						<div class="agile-status">&nbsp;<?php echo $s->st;?></div>
						<div class="agile-amount">&nbsp;<?php echo $s->amount;?></div>
						<div class="agile-amount">
							<?php 
								$sql11 = "SELECT * FROM {$wpdb->prefix}agile_refund_installment WHERE row_id = {$s->Id}";
								$activity_refund = $wpdb->get_row($sql11);
							?>
							<input type = "hidden" id = "id_amount_<?php echo $s->Id;?>" value = "<?php echo $s->amount;?>" />
							<input type = "hidden" id = "id_payment_<?php echo $s->Id;?>" value = "Payment<?php echo $s->installment;?> / <?php echo $count;?>" />
							<input type = "hidden" id = "id_activity_<?php echo $s->Id;?>" value = "<?php echo $s->Id;?>" /><a Onclick = "agile_appear_id(<?php echo $s->Id;?>)">Refund</a>
						</div>
					</div>
					<?php if(!empty($activity_refund)){?>
								<div class="agile-table">
									<?php 
										$dt111= new datetime($activity_refund->dt);
										$dt2=$dt111->format('Y-m-d');
									?>
									<div class="agile-due"><?php echo $dt2;?></div>
									<div class="agile-as"><?php echo $activity_refund->installment;?></div>
									<div class="agile-auto">&nbsp;</div>
									<div class="agile-from">&nbsp;</div>
									<div class="agile-for">&nbsp;</div>
									<div class="agile-status">&nbsp;<?php echo $activity_refund->status;?></div>
									<div class="agile-amount">&nbsp;<?php echo $activity_refund->amount;?></div>
								</div>
					<?php
							}
				} 	?>
				<script>
					jQuery(document).ready(function () {
						jQuery('#show_div').hide('fast');
					});
					function agile_appear_id(id){
						jQuery('#show_div').show('fast');
						var hv = jQuery('#id_activity_'+id).val(); 
						var hm = jQuery('#id_amount_'+id).val(); 
						var hp = jQuery('#id_payment_'+id).val(); 
						jQuery("#append_amount").val(hv);
						jQuery("#append_id").val(hm);
						jQuery("#append_payment").val(hp);	  
					}
				</script>
			<div class="agile-table" id = "show_div">
				<form method = "post" action = "">
					<div class="agile-activity-amount">Refund Payment</div>
					<div class="agile-activity-amount">
						<input style= "width:5em;" type = "text" name = "refund_amount_pay" value = "" id = "append_id"/>
					</div>
					<div class="agile-activity-amount">
						<input style= "width:5em;" type = "text" name = "refund_id" value = "" id = "append_amount"/>
					</div>
					<div class="agile-activity-amount">
						<input style= "width:8em;" type = "text" name = "refund_payment" value = "" id = "append_payment"/>
					</div>
					<div class="agile-activity-amount">
						<select name = "refund">
							<option value = "Refund">Refund</option>
						</select>
					</div>
					<div class="agile-activity-amount">
						<input class = "button-primary" type = "submit" name = "refund_amount" value = "Refund">
					</div>
				</form>
			</div>
			</div><!--content end -->
		<?php
		}
		function agile_admin_show_payment(){
			$o_id=$_GET['id'];
			$row=$this->agile_show_outstandin_installment($o_id);
			$get=$this->get_total_installment($o_id);
				foreach($row as $r){
					$user_id_chk_attmp_transac = $r->user_id;
					$email=get_post_meta($r->order_id);
					$mail=$email['_billing_email']['0'];
					$data = get_user_by('email',$mail );
					$user_name=$data->user_nicename;
					break;
				}
				$max_get_value = get_option('_agile_max_failed_attempt_trans');
				$meta_count_attmp = get_user_meta($user_id_chk_attmp_transac , 'agile_count_attmp_failed_transc');
				$url_link =admin_url('admin.php?page=agile_settings_reset_user_failed_trans', __FILE__);
				if($max_get_value >=  $meta_count_attmp){
					?>
					<div class = "error"> There are repeated declines for this Profile/Credit Card, Will not attempt again.  <a href = "<?php echo $url_link.'&&user_id='.$user_id_chk_attmp_transac;?>">Click Here to Reset</a></div>
					<?php
				 }
			?>
			<div class="agile-heading"><h3>Payment From &nbsp;<?php echo $user_name; ?></h3></iv>
			<div class="agile-total">
				<div class="agile-pay_total">Total Amount</div>
				<div class="agile-amount_total"><?php echo $get;?></div>
			</div>
			<div class="agile-content">
				<div class="agile-header">
					<div class="Payment_agile-due">Paid</div>
					<div class="payment_agile-auto">As</div>
					<div class="payment_agile-as">Order No</div>
					<div class="payment_agile-from">Product</div>
					<div class="payment_agile-for">Ref. No</div>
					<div class="payment_agile-amount">Amount</div>
				</div>
				<div class="agile-table">
				<?php
				$add=0;
				foreach($row as $r){
							$y=$r->amount;
							$add=$y+$add;
							$x = get_product($r->prod_id);
							$title=$x->get_title();
							$orderid=$r->order_id;
							$prodid=$r->prod_id;
							$user_id=$r->user_id;
							$count= $this->get_inst_count($orderid, $prodid);
							$profile=get_user_meta( $user_id, '_wc_authorize_net_cim_profile_id');
							$customer_profile_id=$profile['0'];
						?>
						<div class="agile-table">
						<div class="Payment_agile-due"><?php echo $r->paydt;?></div>
						<div class="payment_agile-auto"> payment&nbsp;&nbsp;<?php  echo $r->installment;?> of <?php echo $count;?></div>
						<div class="payment_agile-as"><?php  echo $r->order_id;?></div>
						<div class="payment_agile-from"><?php  echo $title;?></div>
						<div class="payment_agile-for">&nbsp;<?php  echo $customer_profile_id;?></div>
						<div class="payment_agile-amount"><?php  echo $r->amount;?></div>
						</div>
					<?php
				} 	?>
				</div>
			</div>
			<?php
			$this->admin_show_installment($o_id);
		}
		function agile_download_csv(){
			$results=$this->agile_schedule_installment($o_id);
			if(count($results)>0){
				header("Content-type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"agile_installment.csv\"");
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');

				echo "Due Date, Auto, Paid, Paid Date, Description, Amount \r\n"; 
				
				foreach($results as $r){
				  echo $r->duedt.','.$r->duedt.','.$r->st.','.$r->paydt.','.$r->installment.','.$r->amount.'\r\n';	
				}
			
			}else{
				echo "No Records Available";
			}
		}
		function confirm_pay($row_id){
			$show = $this->arrange($row_id);
			$x = get_product($show->prod_id);
			$title = $x->get_title();
			?>
			
				<form method="post" action="">
				<div>Are You Sure You Want To Pay Now for this installment?&nbspFor your:</div>
				<div class="agile-content" style = "text-align:left; border:1px solid black; padding:5px; ">
							<div class="agile-content">
								<div class="agile-front">Order Id:</div>
								<div class="agile-front"><?php echo $show->order_id;?></div>
							</div>
							<div class="agile-content">
								<div class="agile-front">Installment No:</div>
								<div class="agile-front"><?php echo $show->installment;?></div>
							</div>
							<div class="agile-content">
								<div class="agile-front">Product Name:</div>
								<div class="agile-front"><?php echo $title;?></div>
							</div>
				</div>
					<div class="agile-link-front" style = "width:auto; padding:5px;">
						<input type="hidden" value="<?php echo $row_id;?>" name="rowid"/>
						
						<input style="margin:3px;" type="submit" name="payconfirm" value="Proceed"/>
						
						<input style="margin:3px;"type="button" name="cancel" value="Cancel" onclick="javascript:history.back();"/> <?php //TODO location.href to prev page?>
					</div>
				</form>
				
				
			<?php
		}
		function agile_pay_now($row_id){
			$user_id = get_current_user_id();
			$order = $this->agile_pay_now_installment($row_id);
			$matchids = $this->agile_match_rowid_userid($row_id,$user_id);
			if($matchids == 1){
				
				$profile=get_user_meta( $user_id, '_wc_authorize_net_cim_profile_id');
				$customer_profile_id=$profile['0'];
				$profile_payment=get_user_meta( $user_id,'_wc_authorize_net_cim_payment_profiles');
				$profile_id=$profile_payment['0'];
				foreach($profile_id as $key=>$id){
					$payment_profile_id=$key;
				}
				$get_txid = get_option( 'woocommerce_authorize_net_cim_settings' );
				$mode = $get_txid['test_mode'];
				if($mode=='yes'){
					$environment='test';
					$api_transaction_key=$get_txid['test_api_transaction_key'];
					$api_user_id = $get_txid['test_api_login_id'];
				}else{
					$environment = 'production';
					$api_transaction_key=$get_txid['api_transaction_key'];
					$api_user_id = $get_txid['api_login_id'];
				}
				$new['api_user_id'] = $api_user_id;
				$new['api_transaction_key'] = $api_transaction_key;
				$new['environment'] = $environment;
				$new['order'] = $order;
				$new['payment_profile_id'] = $payment_profile_id;
				$new['customer_profile_id'] = $customer_profile_id;
				$detail['gateway_payment_deduct_request_data'] = $new; 
				$this->log_trace($mail, $order[0]->order_id, $detail);
				$agile_class_gateway = new WC_Authorize_Net_CIM_API_TO($api_user_id,$api_transaction_key,$environment);	
				//$resp = $agile_class_gateway->create_single_transaction($order);
				$resp = $agile_class_gateway->create_new_transaction($order,$payment_profile_id,$customer_profile_id);
				$code = $resp->messages;
				$code1 = $code->resultCode;
				$text = $code->message->text;
				if($code == 'ok' || $text == 'Successful.'){
					$this->update_total_installment_pay_now($row_id);
					echo "Paid Successfully";
				}else{
					echo "An Error Occurred While Paying The Installment";
				}
				$email=get_post_meta($order[0]->order_id);
				$mail=$email['_billing_email']['0'];
				$old['gateway_payment_deduct_request_self_front'] = $resp;
				$this->log_trace($mail, $order[0]->order_id, $old);
			}else{
				echo "Your Data Did Not Matched";
			}
		}
		function front_show_installment(){
			$detail = array();
			$detail['paynow'] = $_POST;
			$user_id = get_current_user_id();
			$user_id = $user_info = get_userdata($user_id);
			$email = $user_info->user_email;
			$this->log_trace($email, $order_id, $detail);
			if(isset($_POST['paynow'])){
				$row_id=$_POST['rowid'];
				$this->confirm_pay($row_id);
				return;
			}
			if(isset($_POST['payconfirm'])){
				$row_id=$_POST['rowid'];
				$this->agile_pay_now($row_id);	
			}
			$user_id = get_current_user_id();
			$max_get_value = get_option('_agile_max_failed_attempt_trans');
			$meta_count_attmp = get_user_meta($user_id , 'agile_count_attmp_failed_transc');
				if($max_get_value >=  $meta_count_attmp){
					?>
					<div style = "clear:left;float:left; color:red; background-color:white;"> There are repeated declines for this Profile/Credit Card, Will not attempt again.</div>
					<?php
				 }
			if($user_id == 0){
				$login_url = home_url().'/wp-login.php';
				return '<a href = '.$login_url.'>Please Log In Here</a>';
				
			}
			$show=$this->agile_show_front_installment($user_id);
			$o_id = $show[0]->order_id;
			$results = $this->agile_schedule_installment_history($o_id);
			if(isset($_POST['activity_user'])){
				?>
					<h3 style = "float:left; clear: left;">Activity</h3>
					<div class="agile-content"><!--content start -->
					<div class="agile-header">
						<div class="agile-due">Order No</div>
						<div class="agile-as"><?php echo $o_id; ?></div>
						<div class="agile-auto"></div>
						<div class="agile-from"></div>
						<div class="agile-for"></div>
						<div class="agile-status"></div>
						<div class="agile-amount"></div>
						<div class="agile-amount">&nbsp;</div>
					</div>
					<div class="agile-header">
						<div class="agile-due">Create Date</div>
						<?php $date = $show[0]->dt;?>
						<?php $newdt =  explode(' ',$date);?>
						<?php $dt_n = $newdt[0];?>
						<div class="agile-as"><?php echo $dt_n; ?></div>
						<div class="agile-auto"></div>
						<div class="agile-from"></div>
						<div class="agile-for"></div>
						<div class="agile-status"></div>
						<div class="agile-amount"></div>
						<div class="agile-amount">&nbsp;</div>
					</div>
					<div class="agile-header">
						<div class="agile-due">paid Date</div>
						<div class="agile-as">Payment</div>
						<div class="agile-auto">&nbsp;</div>
						<div class="agile-from">From</div>
						<div class="agile-for">Product</div>
						<div class="agile-status">Status</div>
						<div class="agile-amount">Amount</div>
						<div class="agile-amount">&nbsp;</div>
					</div>
				<?php
				//zeeshan
					foreach($results as $s){
						$email=get_post_meta($s->order_id);
						$mail=$email['_billing_email']['0'];
						$data = get_user_by('email',$mail );
						$user_name=$data->user_nicename;
						$x = get_product($s->prod_id);
						$x=$x->post;
						$title=$x->post_title;
						$orderid=$s->order_id;
						$prodid=$s->prod_id;
						$count= $this->get_inst_count($orderid, $prodid);
						?>
						<div class="agile-table">
							<div class="agile-due"><?php echo $s->paydt;?></div>
							<div class="agile-as-act">Payment&nbsp;&nbsp;<?php echo $s->installment;?> / <?php echo $count;?> </div>
							<div class="agile-auto">&nbsp;<?php//  echo $s->auto;?></div>
							<div class="agile-from">&nbsp;<?php echo $user_name;?></div>
							<div class="agile-for">&nbsp;<?php echo $title;?></div>
							<div class="agile-status">&nbsp;<?php echo $s->st;?></div>
							<div class="agile-amount">&nbsp;<?php echo $s->amount;?></div>
						</div>
						<?php
					}
					return ;
				//zeeshan
			}
		?>
			<div class="agile-heading"><h3 style = "float:left;clear:left;"> Outstanding Payment</h3></div>
			<div class="agile-content">
				<div class="agile-header">
					<div class="agile-due">Due</div>
					<div class="agile-auto">Auto</div>
					<div class="agile-as">As</div>
					<div class="agile-from">From</div>
					<div class="agile-for">For</div>
					<div class="agile-status">Status</div>
					<div class="agile-amount">Amount</div>
					<div class="agile-link"></div>
				</div>
				<div class="agile-table">
				<?php	
				foreach($show as $s){
							$email=get_post_meta($s->order_id);
							$mail=$email['_billing_email']['0'];
							$data = get_user_by('email',$mail );
							$user_name=$data->user_nicename;
							$x = get_product($s->prod_id);
							$title=$x->get_title();
							$orderid=$s->order_id;
							$prodid=$s->prod_id;
							$count= $this->get_inst_count($orderid, $prodid);	
				?>
					<div class="agile-table">
						<form method="post" action="">
							<div class="agile-due"><?php echo $s->duedt;?></div>
							<div class="agile-auto"><?php if($s->auto=='Yes'){ echo $s->auto;}else{ echo 'No';}?></div>
							<div class="agile-as">Payment&nbsp;&nbsp;<?php echo $s->installment;?> of <?php echo $count;?></div>
							<div class="agile-from"><?php echo $user_name;?></div>
							<div class="agile-for"><?php echo $title;?></div>
							<div class="agile-status"><?php echo $s->st;?></div>
							<div class="agile-amount"><?php echo $s->amount;?></div>
							<div class="agile-link"><input style="margin:4px;" type="submit" name="paynow" value="Pay Now"/></div>
							<input type="hidden" value="<?php echo $s->Id;?>" name="rowid"/>
						</form>
					</div>
					<?php } ?>
				</div>
			</div>
			<div style = "clear:left; float: left;">
				<form method = "post" action = "" >
					<input type = "submit" name = "activity_user" value = "Activity" />
				</form>
			</div>
			<?php
		}
		function admin_show_installment($o_id){
			$show=$this->agile_show_due_installment($o_id);
			
			?>
			<div class="agile-heading"><h3> Outstanding Payment</h3></div>
			<div class="agile-content">
				<div class="agile-header">
					<div class="agile-due">Due</div>
					<div class="agile-as">As</div>
					<div class="agile-auto">Auto</div>
					<div class="agile-from">From</div>
					<div class="agile-for">Product</div>
					<div class="agile-status">Status</div>
					<div class="agile-amount">Amount</div>
					<div class="agile-link"></div>
				</div>
				<div class="agile-table">
				<?php 	
						$url = home_url();
						$link = $url.'/wp-admin/admin.php?page='.'agile_schedule_payment';
						foreach($show as $s){
							$email=get_post_meta($s->order_id);
							$mail=$email['_billing_email']['0'];
							$data = get_user_by('email',$mail );
							$user_name=$data->user_nicename;
							$x = get_product($s->prod_id);
							$title=$x->get_title();
							$orderid=$s->order_id;
							$prodid=$s->prod_id;
							$count= $this->get_inst_count($orderid, $prodid);
							//
							
							
							//
				?>
					
							<div class="agile-table">
							<div class="agile-due"><?php echo $s->duedt;?></div>
							<div class="agile-as">Payment&nbsp;&nbsp;<?php echo $s->installment;?> of <?php echo $count;?> </div>
							<div class="agile-auto"><?php if($s->auto=='Yes'){ echo $s->auto;}else{ echo 'No';}?></div>
							<div class="agile-from"><?php echo $user_name;?></div>
							<div class="agile-for"><?php echo $title;?></div>
							<div class="agile-status"><?php echo $s->st;?></div>
							<div class="agile-amount"><?php echo $s->amount;?></div>
							<div class="agile-link">
								<a onclick="return pnconfirm();" href="<?php echo admin_url().'admin.php?page=agile_ip_paynow&id='.$s->Id; ?>">Mark As Paid</a>|<a href="<?php echo $link.'&id='.$s->order_id; ?>">Edit</a>  
							</div>
							</div>
							<script>
								function pnconfirm(){ 
									var r = confirm("Are you sure to mark it as PAID");
									if (r == true){
										return true;
									}else{
										return false;
									}
								}
					</script>
				<?php	 } ?>	
				</div>
			</div>
			<?php
		}
		function processing_complete_order_status( $order_status, $order_id ) {
			  //$order = new WC_Order( $order_id );
			  return $order_status;
		}
		function scripts_method(){
			wp_enqueue_style( 'agile_install_admin_style', plugins_url('css/admin.css', __FILE__) );
			
		}
		function frontend_scripts(){
			wp_enqueue_style( 'agile_install_front_style', plugins_url('css/front.css', __FILE__) );
		}
		function admin_footer() {
			
		}
		
		function item_data( $data, $cart_item ) {
			if ( isset( $cart_item['pay'] ) ) {
				$data['pay'] = array('name' => 'Pay', 'value' => $cart_item['pay']);
				$data['remi'] = array('name' => 'Remi', 'value' => $cart_item['remi']);
				$data['Id'] = array('name' => 'Auto', 'value' => $cart_item['Id']);
			}
			return $data;
		}
		function add_item_meta( $item_id, $values ) {
			$total_amount_product_cart = 0;
			foreach ( WC()->cart->get_cart() as $code ){
				$line_total = $code['line_total'];
				$total_amount_product_cart = $total_amount_product_cart + $line_total;
			}
				$cart_total =  WC()->cart->total;
				$coupon_amount = $total_amount_product_cart - $cart_total;
			foreach ( WC()->cart->get_cart() as $item ){
				
				
			}
			$line_subtotal = $values['line_subtotal'];
			$prec_prod_amount = ($line_subtotal/$total_amount_product_cart)*100;
			$prec_prod_amount = round($prec_prod_amount , 2);
			$per_product_coupon_amount = ($prec_prod_amount/100)*$coupon_amount;
			$per_product_coupon_amount = round($per_product_coupon_amount , 2);
			woocommerce_add_order_item_meta( $item_id, 'total_coupon_amount',$per_product_coupon_amount);
			woocommerce_add_order_item_meta( $item_id, 'pay', $values['pay'] );
			woocommerce_add_order_item_meta( $item_id, 'remi', $values['remi'] );
			woocommerce_add_order_item_meta( $item_id, 'auto', $values['Id'] );
		}
		function my_columns_function($columns){
			$new_columns = array();
			foreach($columns as $k=>$v){
				$new_columns[$k] = $v;
				//if($new_columns[$k] == 'order_items'){
				if($k == 'order_items'){
					$new_columns['outstanding'] = 'Outstanding';
					$new_columns['enter_installments'] = 'Enter Installments';
				}
			}
			return $new_columns;
		}
		function my_columns_values_function($column){
			global $post;
			
			$order_id=$post->ID;
			$url =admin_url('admin.php?page=agile_show_users_payment', __FILE__);
			$due=$this->get_total_due($order_id);
			if ( $column == 'outstanding' ) {    
				?>
					<a href="<?php echo $url.'&&id='.$order_id;?>"><?php if(!empty($due)){ echo $due;}else{echo '0';}?></a>
				<?php
			}
			if(empty($due)){
				if ( $column == 'enter_installments' ) {    
					$url_link =admin_url('admin.php?page=agile_show_enter_amount', __FILE__);
					?>
						<a href="<?php echo $url_link.'&&id='.$order_id;?>">Enter Amount</a>
					<?php
				}
			}
		}
		
		function get_result_from_searching_email($search){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE email = '{$search}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_result_from_searching($search_order, $search_email){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE email = '{$search_email}' AND orderid  = '{$search_order}'";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function get_result_from_searching_orderno($search){
			global $wpdb;
			
			$sql = "SELECT * FROM {$wpdb->prefix}_aspk_trace_log WHERE orderid = {$search}";
			$rs = $wpdb->get_results($sql);
			return $rs;
		}
		
		function log_trace($email, $orderid, $detail){
			$result = $this->insert_log_trace_db($email, $orderid, $detail);
		}
		

		function insert_log_trace_db($email, $orderid=0, $detail){
			global $wpdb;
			
			$date = new datetime();
			$dt = $date->format('Y-m-d H:i:s');
			$old = serialize ($detail);
			$sql = "INSERT INTO {$wpdb->prefix}_aspk_trace_log(dt,email,orderid,detail) VALUES('{$dt}','{$email}','{$orderid}','{$old}')";
			$wpdb->query($sql);	
		}
		
		function install(){
			global $wpdb;
			
			$sql="
				CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."_aspk_trace_log` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `dt` date NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `orderid` int(11) NULL,
				  `detail` TEXT NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$wpdb->query($sql);
			
			$sql="
				CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."installment_process` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `dt` date NOT NULL,
				  `orderid` int(11) NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
			";
			$wpdb->query($sql);
			
			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			$sql="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."agile_ip_installment` (
			  `Id` int(11) NOT NULL AUTO_INCREMENT,
			  `user_id` int(11) NOT NULL,
			  `amount` decimal(30,2) NOT NULL DEFAULT '0',
			  `txid` varchar(60) DEFAULT NULL,
			  `order_id` int(11) NOT NULL,
			  `prod_id` int(11) NOT NULL,
			  `installment` int(10) NOT NULL,
			  `auto` varchar(3) NOT NULL,
			  `pay_mthd` varchar(30) DEFAULT NULL,
			  `dt` datetime DEFAULT NULL,
			  `duedt` date DEFAULT NULL,
			  `paydt` date DEFAULT NULL,
			  `st` varchar(30) DEFAULT NULL COMMENT 'due,paid',
			  PRIMARY KEY (`Id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
			dbDelta($sql);
			$modify_tbl = get_option('_agile_ip_db',0);
			if($modify_tbl == 0){
				$sql = "alter table `".$wpdb->prefix."agile_ip_installment` modify `amount` decimal(30,2);";
				$retrn = $wpdb->query($sql);
				if($retrn){
					update_option('_agile_ip_db',2);
				}
				
			}	
			$sql = "CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."agile_refund_installment` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `row_id` int(30) NOT NULL,
			  `dt` datetime NOT NULL,
			  `amount` varchar(30) NOT NULL,
			  `installment` varchar(30) NOT NULL,
			  `status` varchar(30) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
			dbDelta($sql);
			$sql_log = "CREATE TABLE IF NOT EXISTS`".$wpdb->prefix."agile_log_installment_get_cron` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `date` datetime NOT NULL,
			  `data` text(5000) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
			dbDelta($sql_log);
				update_option('agile_ip_dbversion',1);
			//crown handle
			wp_clear_scheduled_hook( 'my_task_hook' );
			if ( ! wp_next_scheduled( 'my_task_hook' ) ) {
				//$dt= date('Y-m-d 01:00:00',time()+86400);
				//$start = strtotime($dt) - (60*60*5);
				$date = new DateTime(null, new DateTimeZone('Canada/Saskatchewan'));
				$date2 = DateTime::createFromFormat('Y-m-d h:i',$date->format('Y-m-d').'01:05');
				$start = $date2->getTimestamp();
				//wp_schedule_event( $start, 'daily', 'my_task_hook' );
				wp_schedule_event( $start, 'hourly', 'my_task_hook' );
				
			}
			$start_time = time();
			wp_schedule_event( $start_time, 'minutes_5', 'prefix_hourly_event_hook' );
		}
		function de_activate(){
			wp_clear_scheduled_hook( 'my_task_hook' );
			wp_clear_scheduled_hook( 'prefix_hourly_event_hook' );
		}
		
	} //class ends
} //class exists ends
$aspk_ip = new Agile_Installment_Plan();
?>
