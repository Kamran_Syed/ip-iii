<?php

require_once '../../../wp-load.php';
	$o_id=$_GET['id'];
	$st=$_GET['st'];
	function agile_schedule_installment($o_id){
			global $wpdb;
			
			$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment` WHERE order_id={$o_id}";
			$rs=$wpdb->get_results($sql); 
			return $rs;
	}
	function agile_schedule_all_installment($st){
			global $wpdb;
			
			//$sql = "SELECT * FROM `{$wpdb->prefix}agile_ip_installment`";
			$sql="Select * from {$wpdb->prefix}agile_ip_installment order by order_id desc limit {$st},100 ";
			$rs=$wpdb->get_results($sql); 
			return $rs;
	}

	function get_inst_count($orderid, $prodid){
			 global $wpdb;
			 
			$sql = "SELECT count(`order_id`) FROM {$wpdb->prefix}agile_ip_installment where `order_id`={$orderid} and prod_id={$prodid}";
			$rs= $wpdb->get_var($sql);
			return $rs; 
		}
	if(!empty($o_id)){
		$x=agile_schedule_installment($o_id);
	}else{
		$x=agile_schedule_all_installment($st);
	}
	global $product;
	if(count($x) > 0){
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"timesheet_aggregate_hrs.csv\"");
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');

		echo "WooCommerce ID, User ID, Student Name, Product, As, Amount, Status, Due Date, Paid Date, Auto, SKU \r\n"; 
				foreach($x as $r){
					$orderid=$r->order_id;
					$email=get_post_meta($orderid);
					$mail=$email['_billing_email']['0'];
					$data = get_user_by('email',$mail );
					$user_name=$data->user_nicename;
					$user_fname = $data->user_firstname;
					$user_lname = $data->user_lastname;
					$user_name_display = $user_fname.' '.$user_lname;
					$prodid=$r->prod_id;
						$pord_get = get_product($prodid);
						if(!empty($pord_get)){
							$title = $pord_get->get_title();
						}
					if(!empty($pord_get)){
						$sku_pro = $pord_get->get_sku();
					}
					if($orderid)
					$y = get_inst_count($orderid, $prodid);
					if($r->auto==''){
						$r->auto='No';
					}
					if($r->paydt==''){
						$r->paydt='0000-00-00';
					}
				  echo $orderid.','.$user_name.','.$user_name_display.','.$title.',payment'.$r->installment.'of'.$y.','.$r->amount.','.$r->st.','.$r->duedt.','.$r->paydt.','.$r->auto.','.$sku_pro."\r\n";
				}
	}else{
		echo "No Records Available";
	}
?>