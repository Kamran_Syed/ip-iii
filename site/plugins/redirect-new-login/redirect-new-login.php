<?php

/*
Plugin Name: Rename wp-login.php
Plugin URI: 
Description: Rename the login page.
Version: 1.0
Author: AgileSolutionspk
Author URI: http://agilesolutionspk.com/
*/

if ( defined( 'ABSPATH' )
	&& ! class_exists( 'redirect_new_login' ) ) {

	class redirect_new_login {
		
		public function __construct() {

			add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ), 1 );	
			//add_action( 'site_url', array( $this, 'wpadmin_filter' ), 10, 3 );	
		}
		
		public function plugins_loaded() {

			global $pagenow;
			
			$request = parse_url( $_SERVER['REQUEST_URI'] );
			$url = $this->full_url($_SERVER);
			if  ( strpos($url, 'courses.escoffieronline.com/wp-login.php?action=lostpassword' ) !== false) {
				wp_redirect( site_url('/my-account-2/lost-password/')); 	
			}elseif( strpos( $url, 'courses.escoffieronline.com/wp-login.php' ) && ( strpos( $url, 'wp-admin' ) == false)){
				wp_redirect(  site_url('/login')); 
			}

		}
		
		function url_origin($s, $use_forwarded_host=false){
			$ssl = (!empty($s['HTTPS']) && $s['HTTPS'] == 'on') ? true:false;
			$sp = strtolower($s['SERVER_PROTOCOL']);
			$protocol = substr($sp, 0, strpos($sp, '/')) . (($ssl) ? 's' : '');
			$port = $s['SERVER_PORT'];
			$port = ((!$ssl && $port=='80') || ($ssl && $port=='443')) ? '' : ':'.$port;
			$host = ($use_forwarded_host && isset($s['HTTP_X_FORWARDED_HOST'])) ? $s['HTTP_X_FORWARDED_HOST'] : (isset($s['HTTP_HOST']) ? $s['HTTP_HOST'] : null);
			$host = isset($host) ? $host : $s['SERVER_NAME'] . $port;
			return $protocol . '://' . $host;
		}
		function full_url($s, $use_forwarded_host=false){
			return ($this->url_origin($s, $use_forwarded_host) . $s['REQUEST_URI']);
		}

		
	}

	new redirect_new_login;

}
