<?php
/**
 * Your Inspiration Themes
 * 
 * @package WordPress
 * @subpackage Your Inspiration Themes
 * @author Your Inspiration Themes Team <info@yithemes.com>
 *
* This source file is subject to the GNU GENERAL PUBLIC LICENSE (GPL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.txt
 */

//let's start the game!
require_once('core/load.php');

//---------------------------------------------
// Everybody changes above will lose his hands
//---------------------------------------------


add_action('wp_logout',create_function('','wp_redirect(home_url());exit();'));

add_filter('woocommerce_login_redirect', 'wc_login_redirect');

function wc_login_redirect( $redirect_to ) {
	$redirect_to = 'http://courses.escoffieronline.com/sso';
	return $redirect_to;
}

/*
 * Custom function to adjust date picker range
 *  Code lifted from official docs here: http://docs.woothemes.com/document/checkout-field-editor/
 *  Added by none other than Nick Stefanski
 */
function custom_adjust_datepicker_range () {
	if ( is_checkout() ) {
		wp_enqueue_script( 'jquery' );
?>
<script type="text/javascript">
jQuery( document ).ready( function ( e ) {
	if ( jQuery( '.checkout-date-picker' ).length ) {
		jQuery( '.checkout-date-picker' ).datepicker( 'option', 'changeYear', true );
		jQuery( '.checkout-date-picker' ).datepicker( 'option', 'yearRange', '-110:+0' );
	}
});
</script>
<?php
	}
} // End custom_adjust_datepicker_range()
 
add_action( 'wp_footer', 'custom_adjust_datepicker_range', 50 ); //this is part of date_picker too

?>
