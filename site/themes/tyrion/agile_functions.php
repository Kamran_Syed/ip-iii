<?php
if ( !class_exists( 'Agile_New_Add_Funtion' )){
	class Agile_New_Add_Funtion{	function __construct(){		add_action( 'wp_enqueue_scripts',  array(&$this, 'agile_credit_card_scripts') );	}		function agile_credit_card_scripts(){			wp_enqueue_script('agile_sol_mlt', get_template_directory_uri() . '/js/jquery.form.min.js',	array('jquery'));		}
		function agile_show_msg($cart_item_key, $cart){			$total_amount_product_cart = 0;			foreach($cart as $c){				$line_total=$c['line_total'];				$total_amount_product_cart = $total_amount_product_cart + $line_total;			}			$cart_total =  WC()->cart->total;			$coupon_amount = $total_amount_product_cart - $cart_total;			
			$product = $cart[$cart_item_key];
			$upfrnt_pay = $product['pay'];
			$remaining_installment = $product['remi'];
			$total_payment = $product['line_total'];						$prec_prod_amount = ($total_payment/$total_amount_product_cart)*100;						$prec_prod_amount = round($prec_prod_amount , 2);						$per_product_coupon_amount = ($prec_prod_amount/100)*$coupon_amount;						$total_payment = $total_payment - $per_product_coupon_amount;
			if(!empty($remaining_installment)){
				$remaining_payment = $total_payment - $upfrnt_pay ;
				$per_mnth_pay = $remaining_payment/$remaining_installment ;
				$per_mnth_pay = round($per_mnth_pay , 2);
				$x = 'Your Monthly Installment is $'.$per_mnth_pay;
			}
			return $x;
		}
	} //class ends
} //class exists ends
$aspk_woo_add_func = new Agile_New_Add_Funtion();