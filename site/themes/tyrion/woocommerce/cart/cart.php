<?php
/**
 * Cart Page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
global $woocommerce;
global $aspk_woo_add_func;


wc_print_notices();
?>
<?php foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) { ?>
						<?php $prod_id=$values['product_id'];
							$prod_value=get_post_meta($prod_id);
							foreach($prod_value as $key=>$v){
								if($key=='Installments'){
									$prod_install_value=$v['0'];
								}
								if($key=='Initial Payment'){
									$Initial_Payments=$v['0'];
								}
							}
							
						?>
		<?php } ?>
<?php do_action( 'woocommerce_before_cart' ); ?>
<h3><?php _e( 'SHOPPING CART', 'yit' ); ?></h3>
<?php if(!empty($prod_install_value)){?>
	<div><h5 style = "color:red;">
		Please set initial payment, select installments and then click on “Update Cart”
	</h5></div>
<?php } ?>
<div class="row-fluid">

    <form action="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" method="post">
    <div class="span9 cart-list">
		<div class="span12" style="overflow-x: auto;">

        <?php do_action( 'woocommerce_before_cart_table' ); ?>
        <table class="shop_table cart" cellspacing="0">
            <thead>
            <tr>
                <th class="product-remove"></th>
                <th class="product-name"><?php _e( 'Description', 'yit' ); ?></th>
                <th class="product-quantity"><?php _e( 'Quantity', 'yit' ); ?></th>
				<?php if(!empty($prod_install_value)){ ?>
						<th class="product-quantity"><?php _e( 'Initial Payment', 'woocommerce' ); ?></th>
						<th class="product-quantity"><?php _e( 'No. Of Installments', 'woocommerce' ); ?></th>
				<?php }else{ ?>
					<th class="product-quantity">&nbsp;</th>
					<th class="product-quantity">&nbsp;</th>
				<?php } ?>
                <th class="product-subtotal"><?php _e( 'Subtotal', 'yit' ); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php do_action( 'woocommerce_before_cart_contents' ); ?>

            <?php
			//item id suffix
			$nn = 0;
            if ( sizeof( $woocommerce->cart->get_cart() ) > 0 ) {
                foreach ( $woocommerce->cart->get_cart() as $cart_item_key => $values ) {
                    $_product = $values['data'];
					$nn++;
                    if ( $_product->exists() && $values['quantity'] > 0 ) {
                        ?>
                        <tr class = "<?php echo esc_attr( apply_filters('woocommerce_cart_table_item_class', 'cart_table_item', $values, $cart_item_key ) ); ?>">

                            <!-- Remove from cart link -->
                            <td class="product-remove">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove" title="%s">&times;</a>', esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ), __( 'Remove this item', 'yit' ) ), $cart_item_key );
                                ?>
                            </td>
                            <!-- Product Name -->
                            <td class="product-name">
								<div style = "float:left; clear:left;"> <!-- row -->
									<!-- The thumbnail -->
									<div class="product-thumbnail" style = "float:left;5em;">
										<?php
										$thumbnail = apply_filters( 'woocommerce_in_cart_product_thumbnail', $_product->get_image(), $values, $cart_item_key );

										if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )
											echo $thumbnail;
										else
											printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), $thumbnail );
										?>
									</div>
									<div style = "float:left; width:25em;"><!-- colomn -->
										<div class="product-name-price">
											<div class="product-name">
												<?php
												if ( ! $_product->is_visible() || ( ! empty( $_product->variation_id ) && ! $_product->parent_is_visible() ) )
													echo apply_filters( 'woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key );
												else
													printf('<a href="%s">%s</a>', esc_url( get_permalink( apply_filters('woocommerce_in_cart_product_id', $values['product_id'] ) ) ), apply_filters('woocommerce_in_cart_product_title', $_product->get_title(), $values, $cart_item_key ) );

												// Meta data
												//echo $woocommerce->cart->get_item_data( $values );

												// Backorder notification
												if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $values['quantity'] ) )
													echo '<p class="backorder_notification">' . __( 'Available on backorder', 'yit' ) . '</p>';
												?>
											</div>


											<!-- Product price -->
											<div class="product-price" style = "float:left; clear:left;">
												<?php
												$product_price = get_option('woocommerce_tax_display_cart') == 'excl' ? $_product->get_price_excluding_tax() : $_product->get_price_including_tax();

												echo apply_filters('woocommerce_cart_item_price_html', wc_price( $product_price ), $values, $cart_item_key );
												?>
											</div>
											<div style = "float:left; clear:left;">
											<?php echo $aspk_woo_add_func->agile_show_msg($cart_item_key, $woocommerce->cart->get_cart() );  ?>
											</div>
										</div>
									</div><!-- End colomn -->
								</div> <!-- End row -->
                            </td>


                            <!-- Quantity inputs -->
                            <td class="product-quantity">
                                <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {

                                    $step	= apply_filters( 'woocommerce_quantity_input_step', '1', $_product );
                                    $min 	= apply_filters( 'woocommerce_quantity_input_min', '', $_product );
                                    $max 	= apply_filters( 'woocommerce_quantity_input_max', $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(), $_product );

                                    $product_quantity = sprintf( '<div class="quantity"><input type="number" name="cart[%s][qty]" step="%s" min="%s" max="%s" value="%s" size="4" title="' . _x( 'Qty', 'Product quantity input tooltip', 'yit' ) . '" class="input-text qty text" maxlength="12" /></div>', $cart_item_key, $step, $min, $max, esc_attr( $values['quantity'] ) );
                                }

                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key );
								$agile_nonce = wp_create_nonce( 'agile_cart' );
								$prod_id=$values['product_id'];
								$prod_value = get_post_meta($prod_id, 'Installments');
								$prod_payment_value = get_post_meta($prod_id, 'Initial Payment');
								$prod_install_value = $prod_value['0'];
								$Initial_Payments=$prod_payment_value['0'];
								if(!empty($prod_install_value)){
							
							?>
					</td>
					<input type="hidden" name="agile_nonce" value="<?php echo $agile_nonce;?>" />
					<!-- paying now -->
					<td class="product-quantity" style="padding-top:12px; padding-left:10px" >
						<script>
							function myBilla(){
								for (i = 1; i <= 20; i++) {
									var x = parseFloat(jQuery("#initial_val_"+i).val());
									var y = parseFloat(jQuery("#initial_pay_"+i).val());
									if(y < x){
										alert('Please Enter Amount Above  '+<?php echo $Initial_Payments; ?> );
									}
								}										
							}
							
						</script>
						<input type="hidden" id="initial_val_<?php echo $nn; ?>" value="<?php echo $Initial_Payments;?>" />
						<input type="text" style="width:5em; height:19px;" placeholder="<?php echo $Initial_Payments; ?>" title="Pay" name="cart[<?php echo $cart_item_key;?>][pay]" size="3" value="<?php echo $values['pay'];?>" id="initial_pay_<?php echo $nn; ?>"  onchange="myBilla()" required />
					</td>
					<!-- Remaining Installment -->
					<td class="product-quantity" style="padding-top:21px; padding-left:10px">
						<?php
						$x = $_POST['cart'];
						$y=$x[$cart_item_key];
						$z=$y['remi'];
						$m = '';
						?>
						<input type="hidden" name="cart[<?php echo $cart_item_key;?>][product_total]"  value="<?php echo $values['line_total'];?>" />
						<select id ="inst_<?php echo $nn;?>" name="cart[<?php echo $cart_item_key;?>][remi]" title="Remi" value="<?php  echo $cart_item['remi'];?>" style= "height:29px; padding:4px;" >
							<option value = "installments"> Installments </option>
							<?php for ($i = 1; $i <= $prod_install_value; $i++) { ?>
								<option <?php if($i == $z){ $m=' selected '; ?>value="<?php echo $i; ?>"<?php echo $m;}?>> <?php echo $i;?></option>
							<?php  } ?>
						</select>
					</td>
							<?php }else{ ?>
								<td class="product-quantity" style="padding-top:12px; padding-left:10px"><input type="hidden" name="cart[<?php echo $cart_item_key;?>][pay]"  value="<?php echo $values['line_total'];?>" /></td>
								<td class="product-quantity" style="padding-top:21px; padding-left:10px">&nbsp;</td>
							<?php } ?>
                            <td class="product-subtotal">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_subtotal', $woocommerce->cart->get_product_subtotal( $_product, $values['quantity'] ), $values, $cart_item_key );
                                ?>
                            </td>
                        </tr>
                    <?php
                    }
                }
            }
            do_action( 'woocommerce_cart_contents' );
            ?>
            <?php do_action( 'woocommerce_after_cart_contents' ); ?>
            </tbody>
        </table>

        <?php do_action( 'woocommerce_after_cart_table' ); ?>
		</div><!--item row new div-->
        <div class="row-fluid border-8">
            <div class="span7">
                <table class="shop_table_shipping shop_table cart" cellspacing="0">

                    <thead>
                        <tr>
                            <th><?php _e( 'Calculate Shipping', 'yit' ); ?> </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td >
                                <?php woocommerce_shipping_calculator(); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <?php if ( $woocommerce->cart->coupons_enabled() ) : ?>
            <div class="span5">
                <table class="shop_table_coupon shop_table cart" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php _e( 'Promotional code', 'yit' ); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php _e('Enter promotion code', 'yit'); ?>" />
                                <input type="submit" class="button" name="apply_coupon" value="<?php _e( 'Apply Code', 'yit' ); ?>" />

                                <?php do_action('woocommerce_cart_coupon'); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php endif ?>

        <?php /*
        <div class="cart-collaterals">

            <?php do_action('woocommerce_cart_collaterals'); ?>

            <?php woocommerce_cart_totals(); ?>

            <?php woocommerce_shipping_calculator(); ?>

        </div>
        */ ?>
        <?php wp_nonce_field( 'woocommerce-cart') ?>
		
	</div>
    
	<div class="span3 cart-user-info">
        <div class="cart-collaterals">
            <?php do_action('woocommerce_cart_collaterals'); ?>
            <?php woocommerce_cart_totals(); ?>
        </div>
    </div>

    </form>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>