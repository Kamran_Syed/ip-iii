<?php
/**
 * My Addresses
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */
global $wp_query;
include_once __DIR__ .'../../../class-wc-authorize-net-cim-api-three.php';
$get_txid = get_option( 'woocommerce_authorize_net_cim_settings' );
	function get_cc_type($cardNumber){
			// Strip non-digits from the number
			$cardNumber = preg_replace('/\D/', '', $cardNumber);

			// First we make sure that the credit
			// card number is under 15 characters
			// in length, otherwise it is invalid;
			$len = strlen($cardNumber);
			if ($len < 15 || $len > 16) {
				throw new Exception("Invalid credit card number: must be 15 or 16 digits.");
			}
			else {
				switch($cardNumber){
					case(preg_match ('/^4/', $cardNumber) >= 1):
						return 'Visa';
					case(preg_match ('/^5[1-5]/', $cardNumber) >= 1):
						return 'Mastercard';
					case(preg_match ('/^3[47]/', $cardNumber) >= 1):
						return 'Amex';
					case(preg_match ('/^3(?:0[0-5]|[68])/', $cardNumber) >= 1):
						return 'Diners Club';
					case(preg_match ('/^6(?:011|5)/', $cardNumber) >= 1):
						return 'Discover';
					case(preg_match ('/^(?:2131|1800|35\d{3})/', $cardNumber) >= 1):
						return 'JCB';
					default:
						throw new Exception("Could not determine the credit card type.");
						break;
				}
			}
		}
	$mode = $get_txid['test_mode'];
	if($mode == 'yes'){
		$environment = 'test';
		$api_transaction_key = $get_txid['test_api_transaction_key'];
		$api_user_id = $get_txid['test_api_login_id'];
	}else{
		$environment = 'production';
		$api_transaction_key=$get_txid['api_transaction_key'];
		$api_user_id = $get_txid['api_login_id'];
	}
	$cust_id = get_current_user_id();
	$profile = get_user_meta( $cust_id, '_wc_authorize_net_cim_profile_id');
	$customer_profile_id = $profile['0']; // customer profile id
	$profile_payment = get_user_meta( $cust_id,'_wc_authorize_net_cim_payment_profiles',true);
	$agile_class_gateway = new WC_Authorize_Net_CIM_API_THREE($api_user_id,$api_transaction_key,$environment); //gateway class call
	
$cc_info = array();
if(isset($_POST['creat_profile'])){
	$cc_info['card_number'] = $_POST['creditcard_auth'];
	$cc_info['expiration_date'] = $_POST['expireyear_auth'].'-'.$_POST['expiremonth_auth'];
	$cc_info['cvv'] = $_POST['secure'];
	$cc_info['type'] = get_cc_type($cc_info['card_number']);
	if(empty($profile)){
		$resp = $agile_class_gateway->create_new_customer( $cust_id, $cc_info,$api_transaction_key,$api_user_id );
		$code = $resp->messages;
		$code1 = $code->resultCode;
		$text = $code->message->text;
		
		if($code == 'ok' || $text == 'Successful.'){
			$c_p_id = (int)$resp->customerProfileId;
			update_user_meta( $cust_id, '_wc_authorize_net_cim_profile_id',$c_p_id);
			$c_p__p_id = (int)$resp->customerPaymentProfileIdList->numericString;
			$profile_detail = array();
			$credit_card = substr($cc_info['card_number'], -4);
			$profile_detail['type'] = $cc_info['type'];
			$profile_detail['last_four'] = 'XXXX'.$credit_card;
			$date = substr($_POST['expireyear_auth'], 2);
			$profile_detail['exp_date'] =	$_POST['expiremonth_auth'].'/'.$date;
			$profile_detail['active'] = 1;
			$pay_pro = array();
			$pay_pro[$c_p__p_id] = $profile_detail;
			update_user_meta( $cust_id,'_wc_authorize_net_cim_payment_profiles',$pay_pro);
			?><div class="woocommerce-message" ><?php echo "Updated Successfully";?></div><?php
		}else{
			?><div class = "woocommerce-error"><?php echo $text; ?></div><?php
		}
	} else{
		$resp = $agile_class_gateway->add_new_payment_profile( $cust_id, $customer_profile_id, $cc_info,$api_transaction_key,$api_user_id );
		$code = $resp->messages;
		$code1 = $code->resultCode;
		$text = $code->message->text;
		if($code == 'ok' || $text == 'Successful.'){
			$c_p__p_id = (int)$resp->customerPaymentProfileId;
			$pay_pro = array();
			$credit_card = substr($cc_info['card_number'], -4);
			$profile_detail['type'] = $cc_info['type'];
			$profile_detail['last_four'] = 'XXXX'.$credit_card;
			$date = substr($_POST['expireyear_auth'], 2);
			$profile_detail['exp_date'] =	$_POST['expiremonth_auth'].'/'.$date;
			$profile_detail['active'] = 1;
			$profile_payment[$c_p__p_id] = $profile_detail;
			update_user_meta( $cust_id,'_wc_authorize_net_cim_payment_profiles',$profile_payment);
			?><div class="woocommerce-message" ><?php echo "Updated Successfully";?></div><?php
		}else{
			?><div class = "woocommerce-error"><?php echo $text; ?></div><?php
		}
		
	}	 
}
if(isset($_POST['delete_profile'])){
		$pp_id = $_POST['pp_id'];
		 $delete_response = $agile_class_gateway->delete_payment_profile( $customer_profile_id, $pp_id,$api_transaction_key,$api_user_id); // delete profile
		$code = $delete_response->messages;
		$code1 = $code->resultCode;
		$text = $code->message->text;
		
		if($code == 'ok' || $text == 'Successful.'){
			delete_user_meta( $cust_id,'_wc_authorize_net_cim_payment_profiles');
			?><div class="woocommerce-message" ><?php echo "Deleted Successfully";?></div><?php
		}else{
			?><div class = "woocommerce-error"><?php echo $text; ?></div><?php
		}
	}
	if(!empty($profile_payment)){
		foreach($profile_payment as $key=>$id){
				?>
				<form method = "post" action = "" style = "border:none;">
					<div style= "float:left; clear:left;">
					<div style = "float:left;margin-left:2em;">
					<select  name = "pp_id" style = "width:10em;">
						<option value = "<?php echo $key; ?>"  selected ><?php echo $id['last_four'] ; ?></option>
						<option value = "<?php echo $key; ?>" ><?php echo $id['type'] ; ?></option>
						<option value = "<?php echo $key; ?>" ><?php echo $id['exp_date']; ?></option>
					</select>
					</div>
					<div style = "float:left;margin-left:2em;"> <input class = "button view" type = "submit" name = "delete_profile" value = "Delete" /></div>
					</div>
				</form>
				<?php
		}
	}
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$customer_id = get_current_user_id();

if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) {
    $page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Addresses', 'yit' ) );
    $get_addresses    = array(
        'billing' => __( 'Billing Address', 'yit' ),
        'shipping' => __( 'Shipping Address', 'yit' )
    );
} else {
    $page_title = apply_filters( 'woocommerce_my_account_my_address_title', __( 'My Address', 'yit' ) );
    $get_addresses    = array(
        'billing' =>  __( 'Billing Address', 'yit' )
    );
}

$col = 1;
?>

    <h2><?php echo $page_title; ?></h2>

<?php if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) echo '<div class="col2-set addresses">'; ?>

<?php $get_address_item = count($get_addresses); $i = 0; ?>

<?php foreach ( $get_addresses as $name => $title ) : ?>
    <?php $i++ ?>
    <div class="address <?php if( $i == $get_address_item ) { echo 'last'; }?>">
        <header class="title">
            <h3><?php echo $title; ?></h3>
        </header>
        <address>
            <?php
            $address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
                'first_name' 	=> get_user_meta( $customer_id, $name . '_first_name', true ),
                'last_name'		=> get_user_meta( $customer_id, $name . '_last_name', true ),
                'company'		=> get_user_meta( $customer_id, $name . '_company', true ),
                'address_1'		=> get_user_meta( $customer_id, $name . '_address_1', true ),
                'address_2'		=> get_user_meta( $customer_id, $name . '_address_2', true ),
                'city'			=> get_user_meta( $customer_id, $name . '_city', true ),
                'state'			=> get_user_meta( $customer_id, $name . '_state', true ),
                'postcode'		=> get_user_meta( $customer_id, $name . '_postcode', true ),
                'country'		=> get_user_meta( $customer_id, $name . '_country', true )
            ), $customer_id, $name );

            $formatted_address = $woocommerce->countries->get_formatted_address( $address );

            if ( ! $formatted_address )
                _e( 'You have not set up this type of address yet.', 'yit' );
            else
                echo $formatted_address;
            ?>
        </address>

        <a href="<?php echo wc_get_endpoint_url( 'edit-address', $name ); ?>" class="edit"><?php _e( 'Edit', 'yit' ); ?></a>


    </div>

<?php endforeach; ?>

<div class="title">
	<div style = "float:left; clear:left;">
	<div style="float:left;"><input type="radio" id="agile_autorize"></div>
	<div style = "float:left;margin-top:4px; margin-left:1em;">Use a New Credit Card Number:</div>
	</div>
</div>
<!-- add credit card field -->
<form method = "post" action = "" style = "border:none;">
	<div id = "agile_form_authorize">
		<div style="background-color:#b4b4b4;float:left; clear:left;padding:5px;margin-top:10px; margin-bottom:1em;"><!--show-div-->
			<div style="clear:left; float:left;width:100%; margin-left:1em;">
				<div style=" float:left; clear:left;">Enter Credit Card Number</div>
				<div style=" float:left;clear:left;">
					<input  style="width:460px;" type="text" name="creditcard_auth" placeholder = "0000 0000 0000 0000" value="" required id ="cc_number" />
					<input type="hidden" name="cust_id" value="<?php echo $customer_id; ?>" />
				</div>
			</div>
			<div style="clear:left; float:left;width:100%; margin-top:5px;">
					<div style=" float:left; width:30%; margin-left:1em;">Expiration Date</div>
					<div style=" float:left; width:30%; margin-left:5em;">Card Security Code</div>
			</div>
			<div style=" float:left;width:40%;">
				<div style=" float:left;width:30%; margin-left:1em;">
					<select type="text" name="expiremonth_auth" >
					<?php //for ($x=01; $x<=12; $x++){ ?>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					<?php // } ?>
					</select>
				</div>
				<?php 
					$newtime =  new datetime();
					$year = $newtime->format('Y');
					$upyear= $year+12;
				?>
				<div style=" float:left;width:40%;">
					<select type="text" name="expireyear_auth" >
					<?php // for ($x=$year+1; $x<=$upyear; $x++){ ?>
					<?php for ($x=$year; $x<=$upyear; $x++){ ?>
						<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div style="float: left;width:30%;">
				<input style="width:70px;margin-left:2em;margin-top:2px;"type="text" name="secure" value="" required />
			</div>
		</div><!--end show-div-->
		<div style="margin: 0 auto; padding:1em; width:45%;clear:left;">
			<input class="css3button" type="submit" name="creat_profile" value="Create Profile" style="padding: 6px 25px; float:left;" id = "button_validt" >
		</div>
	</div>
</form>
<script>
jQuery("#agile_autorize").click(function(){
	jQuery('#agile_form_authorize').show();
	
});
	 jQuery(document).ready(function(){
		jQuery('#agile_form_authorize').hide();
		jQuery("#button_validt").on("click",function(event){
			jQuery('#cc_number').validateCreditCard(function(result){
					if(! result.length_valid){
						alert('Credit Card length is invalid');
						event.preventDefault();
						return false;
					}
					if(! result.luhn_valid){
						alert('Credit Card number is invalid');
						event.preventDefault();
						return false;
					}
					return true;
			});
	   });
	});
</script>
<!-- end credit card field -->

<?php if ( get_option('woocommerce_ship_to_billing_address_only') == 'no' ) echo '</div>'; ?>

